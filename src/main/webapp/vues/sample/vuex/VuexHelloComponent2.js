import store from './Store.js';

new Vue({
  el: '#app',
  store,
  template : `
	     <div>
             <h2>Title : {{ countLinks }} </h2>
             <!-- Add the form here -->
             <form @submit.prevent="addLink">
               <input class="link-input" type="text" placeholder="Add a Link" v-model="newLink" />
             </form>
			<ul>
			  <li v-for="(link, index) in links" v-bind:key="index">
			    {{ link }}
			    <button v-on:click="removeLinks(index)" class="rm">Remove</button>  <!-- Add this -->
			  </li>
			</ul>             
         </div>
  `,
  data() { 
	return { 
		newLink : ''
	}  
  },
  computed: {
	  ...Vuex.mapGetters([ 
		     'countLinks',
		     'links'
	   ])
  },
  components: { },
  methods : {
	  ...Vuex.mapMutations([
		  'ADD_LINK'
	  ]),
	  ...Vuex.mapActions([
		  'removeLink'
	  ]),
	  addLink: function() {
		  this.ADD_LINK(this.newLink);
		  this.newLink = '';
	  },
	  removeLinks : function(link) {
		  this.removeLink(link);
	  }
  }
  
});