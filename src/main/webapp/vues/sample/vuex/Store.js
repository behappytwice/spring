
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    count: 0,
    title : 'My Custom Title ☆☆☆☆☆☆☆☆☆☆☆☆☆☆',
    links: [
    	'http://www.naver.com',
    	'http://www.google.com'
    ]
  },
  getters : {
	  countLinks : state => {
		  return state.links.length;
	  },
	  links : state => {
		  return state.links;
	  }
	 
  },
  mutations : { 
	  ADD_LINK : (state, link) => {
		  state.links.push(link);
	  },
	  REMOVE_LINK: (state, link) => {
		  state.links.splice(link, 1);
	  }
  },
  actions: {
	  removeLink : (context, link) => {
		  context.commit("REMOVE_LINK", link);
	  }
  }
})

export default store;