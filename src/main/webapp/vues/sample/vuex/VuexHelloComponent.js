import store from './Store.js';

new Vue({
  el: '#app',
  store,
  template : `
	     <div>
	                  <h2>Count : {{ count }} </h2>
	                  <h2>Title : {{ title }} </h2>
         </div>
  `,
  //computed: Vuex.mapState([ 'title' ]),
  computed: {
	  ...Vuex.mapState([ 
		     'title',
		     'count'
	   ])
  },

/*  computed: {
	count() {
		//return store.state.count;
		return this.$store.state.count; 
	}  
	  
  },*/
  components: {
  },
  methods : {
	  menuClicked : function() {
		  alert("Menu clicked at boardLeftComponent.");
		  //store.commit('increment');
		  this.$store.commit('increment');
		  //console.log(store.state.count) // -> 1
		  console.log(this.$store.state.count) // -> 1
	  }
  }
});
