export default {
  template: `
    <div>
	     <h1>Single-file JavaScript Component</h1>
	     <p>{{ message }}</p>
	     <h2>User Information</h2>
	     <ul>
		        <li v-for="user in users">
		           {{ user.name }}
		        </li>
		 </ul>
	   	 <v-app>
		    <v-alert      :value="true"      type="info"  >      잘해야 한다. </v-alert>
		    <v-btn color="success">Success</v-btn>
		    <v-btn color="error">Error</v-btn>
		    <v-btn color="warning">Warning</v-btn>
		    <v-btn color="info">Info</v-btn>

			  <v-layout row justify-center>
			    <v-dialog v-model="dialog" scrollable max-width="200px">
			      <v-btn slot="activator" color="primary" dark>Open Dialog</v-btn>
			      <v-card>
			        <v-card-title>Select Country</v-card-title>
			        <v-divider></v-divider>
			        <v-card-text style="height: 200px;">
			          <v-radio-group v-for="(item, index) in users" column>
			            <v-radio label="{{ item.name }} The" value="bahamas"></v-radio>
			          </v-radio-group>
			        </v-card-text>
			        <v-divider></v-divider>
			        <v-card-actions>
			          <v-btn color="blue darken-1" flat @click.native="dialog = false">Close</v-btn>
			          <v-btn color="blue darken-1" flat @click.native="dialog = false">Save</v-btn>
			        </v-card-actions>
			      </v-card>
			    </v-dialog>
			  </v-layout>		    
         </v-app>
    </div>
  `,
  created() {
	this.getUsers();  
  },
  data() {
    return {
    dialogm1:'',
    dialog:false,
      message: 'Oh hai from the component',
      users: []
    }
  },
  methods: {
	  getUsers() {
		  var url = "/spring/service/eval/getUsers";
		  axios.get(url).then(response=> {
			  this.users = response.data;
			  
		  });
	  }
  }
}
