import CompositionLnbComponent from './CompositionLnbComponent.js';
import Utils from '../common/Utils.js';

new Vue({
  el: '#app',
  template : `
     <div>
        <div id="top" v-on:click="test">#top</div>
        <hr>
        <div id="lnb">
           <composition-lnb-component></composition-lnb-component>
        </div>
        <hr>
        <div id="content">
          <composition-content-component></composition-content-component>
        </div>
     </div>
  `,
  components: {
	  CompositionLnbComponent,
	  'composition-content-component': () => import('./CompositionContentComponent.js')
  },
  methods : {
	  test : function() {
		  
		  var config = {
				  url : '/spring/service/vuejs/getJsonEmployee',
				  data : { id:"AXB222SX" },
				  method : 'get'
		  };
		  new Utils().ajax(config, null, null );
	  }
  }
});
