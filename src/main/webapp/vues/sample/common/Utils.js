export default class Utils {

	//https://github.com/axios/axios
	
	constructor() {
		this.config  = {
		   method  : "post",
		   timeout : 10000,
		}
	}// constructor 
	
	//
	success(successCallback){
	}
	// 
	fail(failCallback) {
		
	}
	ajax2() {
		alert("I'm a method of a class");
	}
	
	ajax(config, successCallback, failCallback) {
		//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
		let returnedTarget = Object.assign(this.config, config); // only in ES6, use jQuery in ES5 
		
		// for debugging below 
		console.log(returnedTarget.url);
		console.log(returnedTarget.data);
		console.log(returnedTarget.method);

		/*
		axios(config).then(function(response) {
			alert("succcess");
		})
		.catch(function(error) {
			alert("error");
		})
		*/
	}//ajax 
	
}///~