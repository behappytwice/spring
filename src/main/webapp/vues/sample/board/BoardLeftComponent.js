import EventBus from './EventBus.js';


export default {
  // Left Navitaion Component
  // view section 
  template: `
     <div>
         <h3>게시판</h3>
         <hr>
         <ul>
            <li v-on:click="menuClicked">모든게시함</li>
            <li>공지사항</li>
            <li>자유게시판</li>
         </ul>
        <hr>
        <ul>
           <li v-for="item in userBoardList">
              {{item}}
           </li>
        </ul>
        <h2>Language: {{lang}}</h2>

     </div>
  `,
  // constructor section 
  created() {
  
  },
  // data section 
  data() {
    return {
    	programId : "BOARD02",
    	userBoardList: [ "기능개선아이디어", "개발지식"],
    	// document의 언어값 확인하려고 했는데 값이 안나옴 
    	lang : window.document.title
    }
  },
  // method section 
  methods: {
	  menuClicked : function() {
		  // v-on을 사용한 사용자 지정 이벤트 
		  alert("menuclicked");
		  this.$emit('menuclicked');
		  EventBus.$emit("message", "hello");
	  }
  }
} //export 
