import EventBus from './EventBus.js';


export default {
  // view section 
  template: `
     <div>
         <span>{{article.author}}</span>
         <div v-html="article.contents"></div>
         <br>
         <form>
           사용자명: <input type="text" v-model="article.userName"  id="userNmae" placeholder="값을 입력하세요" value="">
           <input type="button" value="Click Me" v-on:click="check">
         </form>
     </div>
  `,
  // constructor section 
  created() {
	  EventBus.$on('message', this.onReceive);
  
  },
  // data section 
  data() {
    return {
    	programId : "BOARD01",
    	article : {
    		author: "홍길동",
    		contents: "오늘까지만 하기로하고요. <br><h2>다음에 만나면 다시 하죠<h2>",
    		userName :""
    	} 
    }
  },
  // method section 
  methods: {
	  check: function(event) {
		  alert(this.article.userName);
	  },
	  onReceive(text) {
		  alert("Received message:" + text);
	  }
  }
} //export 
