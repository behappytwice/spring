
Vue.use(Vuex);


const store = new Vuex.Store({
  state: {
    count: 0,
    title : 'My Custom Title ☆☆☆☆☆☆☆☆☆☆☆☆☆☆'
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
})


export default store;