import BoardContentComponent from './BoardContentComponent.js';
import BoardLeftComponent from './BoardLeftComponent.js';
import store from './Store.js';

new Vue({
  el: '#app',
  store,
  template : `
	     <div>
	          <!-- 자식 컴포넌트의 이벤트를 받으면 menuClicked() 함수 호출함 -->
	          <board-left-component v-on:menuclicked="menuClicked"></board-left-component>
	          <board-content-component></board-content-component>
	                  <h2>Count : {{ count }} </h2>
	                  <h2>Title : {{ title }} </h2>
         </div>
  `,
  //computed: Vuex.mapState([ 'title' ]),
  computed: {
	  ...Vuex.mapState([ 
		     'title',
		     'count'
	   ])
  },

/*  computed: {
	count() {
		//return store.state.count;
		return this.$store.state.count; 
	}  
	  
  },*/
  components: {
    BoardContentComponent,
    BoardLeftComponent
  },
  methods : {
	  menuClicked : function() {
		  alert("Menu clicked at boardLeftComponent.");
		  //store.commit('increment');
		  this.$store.commit('increment');
		  //console.log(store.state.count) // -> 1
		  console.log(this.$store.state.count) // -> 1
	  }
  }
});
