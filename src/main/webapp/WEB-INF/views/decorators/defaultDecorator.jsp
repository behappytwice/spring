<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/views/core/commonTaglib.jsp" %>
<!DOCTYPE html> 
<html>
 <head>
 <title><sitemesh:write property='title'/></title>
  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.11/handlebars.js"></script>

<%@ include file="/WEB-INF/views/core/commonJavascript.jsp"%> 

 <sitemesh:write property='head'/>

 </head>
 
 <body>
 This is the default body in decorator:
 <sitemesh:write property='body'/>
 </body>
</html>