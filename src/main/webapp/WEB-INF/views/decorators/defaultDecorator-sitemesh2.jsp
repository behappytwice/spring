<%@ page language="java" contentType="text/html; charset=utf-8"    pageEncoding="utf-8"%>
<%-- <%@ include file="/jsp/core/commonTaglib.jsp" %> --%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"     prefix="decorator"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!--  IE9 고정  -->
<meta http-equiv="X-UA-Compatible" content="IE=9">

<title><decorator:title /></title>


<%-- <%@ include file="/jsp/decorators/default/defaultCss.jsp"%> --%>
<%-- <%@ include file="/jsp/decorators/default/defaultJavascript.jsp"%> --%>

<!-- Default Decorator -->


<decorator:head />
</head>
<body
    <decorator:getProperty property="body.id" writeEntireProperty="true"/>
    <decorator:getProperty property="body.class" writeEntireProperty="true"/>
    <decorator:getProperty property="body.onload" writeEntireProperty="true"/>>
    <decorator:body />
</body>
</html>

