<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script>


$(document).ready(function() {
	
	$("#btnEchoEmployee").click(function() {
		var employee = {
				empId : "empId",
				userId : "userId",
				empNo : "empNo",
				userName : "userName"
		};
		$.ajax({
			method : "POST",
			url:  coreProperties.context + "/sample/echoEmployee",
			contentType : 'application/json;charset=utf-8',
			data :  JSON.stringify(employee),
			dataType : "json",
			success : function(dataFromServer) {
				if(dataFromServer) { 
					var response = eval(dataFromServer);
					$("#empId").val( response.empId );
					$("#userId").val( response.userId );
					$("#empNo").val( response.empNo );
					$("#userName").val( response.userName );
				}
			},
			error : function(xhr, except) {
				alert("error");
			}			
		}); // ajax 
		
	}); //  click 
	
	
	$("#btnEmployeeList").click(function() {
		var employee = {
				empId : "empId",
				userId : "userId",
				empNo : "empNo",
				userName : "userName"
		};
		$.ajax({
			method : "POST",
			url:  coreProperties.context + "/sample/employeeList",
			contentType : 'application/json;charset=utf-8',
			data :  JSON.stringify(employee),
			dataType : "json",
			success : function(dataFromServer) {
				if(dataFromServer) { 
					var response = eval(dataFromServer); 
					var empAll = { empList: response }
					
					var source = document.getElementById("template1").innerHTML;
					var template = Handlebars.compile(source);  // precompiled 
					var html = template(empAll);
					$("#displayEmployeeDiv").html(html);
				}
			},
			error : function(xhr, except) {
				alert("error");
			}			
		}); // ajax 
		
	}); //  click 
	
	$("#btnAnotherDomainHtml").click(function() {
		alert("OK");
		$.ajax( {
			url:"http://gw.sanghyun.com:8080/cross/ftlEmployee",
			dataType: "html",
			success: function(data) {
				$("#displayHtml").html(data);
			},
			error: function(xhr) {
				alert("fail");
			}
		});
	}); //  click 
}); 

</script>

</head>

<body>

    <script id="template1" type="text/x-handlebars-template">
      {{#each  empList}}
			<label>아이디</label>
			<input type="text" id="empId"  value="{{empId}}" > <br>
			<label>이름</label>
			<input type="text" id="userName"  value="{{userName}}"> <br>
			<label>이메일</label>
			<input type="text" id="usrId" value="{{userId}}"> <br>
       {{/each}}
	</script>

    <div id="displayEmployeeDiv"></div>
    <div id="displayHtml"></div>
    	
   
   Employee ID: <input type="text" id="empId" value=""><br>
   User ID: <input type="text" id="userId" value=""><br>
   Employee No: <input type="text" id="empNo" value=""><br>
   User Name: <input type="text" id="userName" value=""><br>


   <input type="button" id="btnEchoEmployee" value="Echo Employee" ><br>
   <input type="button" id="btnEmployeeList" value="Get Employee's List" ><br>
   <input type="button" id="btnAnotherDomainHtml" value="Get a HTML of another domain" ><br>
</body>
</html>
