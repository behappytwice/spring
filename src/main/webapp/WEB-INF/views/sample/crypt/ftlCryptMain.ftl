<html>
<body>
<head>

<script src="/resources/lib/rsa/base64.js"></script>
<script src="/resources/lib/rsa/rsa.js"></script>
<script src="/resources/lib/rsa/jsbn.js"></script>
<script src="/resources/lib/rsa/prng4.js"></script>
<script src="/resources/lib/rsa/rng.js"></script>


<script>

$(document).ready(function() {
	$("#btnLogin").click(
			function() {
				var ruserId = $("#userId").val();
				var rpassword = $("#password").val();
				
				if(ruserId == null || ruserId == "")  {
					alert("The userId is empty");
					return; 
				}
				if(rpassword == null || rpassword == "")  {
					alert("The password is empty");
					return; 
				}
				
			
				
				var rsa = new RSAKey();
				rsa.setPublic($("#public-key-module").val(), $("#public-key-exponent").val());
				var loginUser = {
						userId : rsa.encrypt(ruserId),
						password : rsa.encrypt(rpassword)
					
				}
				alert("Send"); 
				
				$.ajax({
					url : "/service/sample/crypt/login",
					method:"POST", 
					contentType : 'application/json;charset=utf-8',
					data : JSON.stringify(loginUser),
					dataType : "json",
					success : function(data) {
						//alert("success" + data);
						alert("UserID:" + data.userId); 
						alert("Password;" + data.password); 
						
					},
					error : function(xhr) {
						alert("fail");
					}
				});

			});	
	
})// ready


</script>
</head>

<div id="header">
	<div>${cryptBean.publicKeyModule}</div>
	<div>${cryptBean.publicKeyExponent}</div>
</div>
<div id="content">
	<form>
		<div>
			User ID: <input type="text" id="userId" value=""> <br>
			Password: <input type="text" id="password" value=""> <br>
		</div>
		<div>
			<input type="hidden" id="public-key-module"
				value="${cryptBean.publicKeyModule}">
		</div>
		<div>
			<input type="hidden" id="public-key-exponent"
				value="${cryptBean.publicKeyExponent}">
		</div>
		<div>
			<input type="button" id="btnLogin" value="클릭하세요">
		</div>
	</form>
</div>

</body>
</html>