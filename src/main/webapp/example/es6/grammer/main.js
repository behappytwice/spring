import { hello } from './mymodule.js';
import { Logger } from './class.js';

(function(window) {
	window.addEventListener("load", function() {
		let logger = new Logger(window);
		hello();
	});
})(window);
