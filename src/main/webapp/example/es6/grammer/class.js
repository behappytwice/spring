
export class Logger { 
	constructor(window) {
		this.window = window;
		this.document = window.document;
		this.name = "Logger";
		this.init();
	}
	init() {
		let hellobtn = this.document.querySelector("#helloBtn");
		console.log(this.name);
		console.log(hellobtn);
		
		hellobtn.addEventListener("click", function() {
			alert("hello");
		});
	}
	debug(msg) {
		this.window.document.write(msg);
	}
}