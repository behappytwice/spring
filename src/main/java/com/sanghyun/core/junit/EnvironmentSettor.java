package com.sanghyun.core.junit;

import java.util.regex.Pattern;

import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.io.ClassPathResource;

import com.sanghyun.core.appconfig.AppConfigConstants;

import ch.qos.logback.classic.util.ContextInitializer;

public class EnvironmentSettor {

	private static void setProfile() {
		if(System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME) == null) { 
			// 지정된 profile이 없는 경우 production 환경을 프로파일로 설정
			System.setProperty(AppConfigConstants.SANGHYUN_PROFILE, AppConfigConstants.PROFILE_DEVELOPMENT);
			System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, AppConfigConstants.PROFILE_DEVELOPMENT);
			
			// 프로파일 경로에 있는 logback.xml 설정
			//System.setProperty( ENV_LOGBACK_CONFIG_FILE, ENV_PRODUCTION);
			ClassPathResource resource = new ClassPathResource("profile/" + AppConfigConstants.PROFILE_DEVELOPMENT + "/logback.xml");
			try { 
				//LogbackConfigurer.initLogging(resource.getFile().getAbsolutePath());
				// profile 경로에 있는 logback.xml을 읽어 들인다. 
				System.setProperty(ContextInitializer.CONFIG_FILE_PROPERTY, resource.getFile().getAbsolutePath());
				System.out.println("#################################################################################");
				System.out.println("#                                                                               #");
				System.out.println("#           개발서버 환경에서 그룹웨어를 시작합니다.                            #");
				System.out.println("#                                                                               #");				
				System.out.println("#################################################################################");
				
			}catch(Exception e) { 
				System.out.println("logback.xml이 지정된 경로에 없습니다.");
			}
		} else {
			
			String profileProperty = System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME);
			
//			if(profileProperty.indexOf(AppConfigConstants.PROFILE_SQLLOG) < 0) { 
//				System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME,  profileProperty + "," + AppConfigConstants.PROFILE_NOSQLLOG);
//			}
			
			String[] profileNames = profileProperty.split(Pattern.quote(","));
			for(String profileName : profileNames)  { 
				if(profileName.trim().equals(AppConfigConstants.PROFILE_SQLLOG)) {
					continue; 
				}else {
							
					if(profileName.trim().equals(AppConfigConstants.PROFILE_PRODUCTION)) {
						System.out.println("#################################################################################");
						System.out.println("#                                                                               #");
						System.out.println("#           운영서버 환경에서 그룹웨어를 시작합니다.                            #");	
						System.out.println("#                                                                               #");
						System.out.println("#################################################################################");
						
					}else if(profileName.trim().equals(AppConfigConstants.PROFILE_STAGING)) {
						System.out.println("#################################################################################");
						System.out.println("#                                                                               #");
						System.out.println("#           검증서버 환경에서 그룹웨어를 시작합니다.                            #");	
						System.out.println("#                                                                               #");
						System.out.println("#################################################################################");						
					}else if(profileName.trim().equals(AppConfigConstants.PROFILE_DEVELOPMENT)) {			
						System.out.println("#################################################################################");
						System.out.println("#                                                                               #");
						System.out.println("#           개발서버 환경에서 그룹웨어를 시작합니다.                            #");	
						System.out.println("#                                                                               #");
						System.out.println("#################################################################################");						

					}else { 
						System.out.println(profileName.trim() + "서버 환경에서 그룹웨어를 시작합니다.");
						System.out.println("#################################################################################");
						System.out.println("#                                                                               #");
						System.out.println("#           " + profileName.trim() + "서버 환경에서 그룹웨어를 시작합니다.                            #");	
						System.out.println("#                                                                               #");
						System.out.println("#################################################################################");						

					}
					
					System.setProperty(AppConfigConstants.SANGHYUN_PROFILE, profileName.trim());
					// profile이 있는 경우 profile/ 아래의 profile 이름의 경로에 있는 logback.xml을 읽는다. 
					ClassPathResource resource = new ClassPathResource("profile/" + profileName.trim() + "/logback.xml");
					try { 
						System.setProperty(ContextInitializer.CONFIG_FILE_PROPERTY, resource.getFile().getAbsolutePath());
					}catch(Exception e) { 
						System.out.println("logback.xml이 지정된 경로에 없습니다.");
					}
					break;
				}
			}
		}
	}//:
	
	public static void setEnvironment(String ...profiles) {
		String profileNames = null;
		for(String profile : profiles) {
			if(profileNames == null) {
				profileNames = profile;
			}else { 
				profileNames += "," + profile;
			}
		}
		if(profileNames != null) {
			System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, profileNames); 	
		}
		setProfile();
		
	}//:
}
