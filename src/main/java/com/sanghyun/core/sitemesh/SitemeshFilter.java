package com.sanghyun.core.sitemesh;

import java.io.File;
import java.io.IOException;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sanghyun.core.util.ClassPathFileUtil;



public class SitemeshFilter extends ConfigurableSiteMeshFilter {

	final static Logger logger = LoggerFactory.getLogger(SitemeshFilter.class);
	
	/**
	 * @see http://wiki.sitemesh.org/wiki/display/sitemesh3/Configuring+SiteMesh+3
	 */
	@Override
	protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
		
//		<excludes>
//		<pattern>/resources/*</pattern>
//		<pattern>*.html</pattern>
//		<pattern>/*.htm</pattern>
//		<pattern>/*.js</pattern>
//		<pattern>/*.css</pattern>
//	    </excludes>
//		builder.addExcludedPath("/resources/*")
//		       .addExcludedPath("*.html")
//		       .addExcludedPath("/*.js")
//		       .addExcludedPath("/*.css");
//		builder.addDecoratorPath("/sample/*" , "/WEB-INF/views/decorators/defaultDecorator.jsp");
//		builder.addDecoratorPath("/view/*"   , "/WEB-INF/views/decorators/defaultDecorator.jsp");
		this.addDecorator("decorator.json", builder);
		this.addDecorator("decorator-extension.json", builder);
	}//:
	
	private void addDecorator(String filePath, SiteMeshFilterBuilder builder) {
		try { 
			File file = ClassPathFileUtil.getFileObject(filePath);
			ObjectMapper objectMapper = new ObjectMapper();
			SitemeshFilterData filterData = objectMapper.readValue(ClassPathFileUtil.getFileObject(filePath), SitemeshFilterData.class);
			if(filterData.getExcludes() != null) {
				
				for(String excludePattern : filterData.getExcludes()) {
					builder.addExcludedPath(excludePattern.trim()); 				
				}
			}
			if(filterData.getDecorators() != null) {
				for(SitemeshDecoratorPath path: filterData.getDecorators()) { 
					builder.addDecoratorPath(path.getContentPath().trim(), path.getDecoratorPath().trim());
				}
			}
		}catch(IOException e) {
			logger.error("decorator.json 파일을 찾을 수 없습니다.");
			throw new RuntimeException(e);
		}catch(Exception e) { 
			throw new RuntimeException(e);
		}
	}//:
	
}///~
