package com.sanghyun.core.sitemesh;

public class SitemeshFilterData {
	
	/**
       사이트 매쉬에서 제외할 패턴 . 해당패턴은 사이트 매쉬를 사용해서 감싸는 대상이 아님
       <pre> 
			<excludes>
				<pattern>/resources</pattern>
				<pattern>*.html</pattern>
				<pattern>/*.htm</pattern>
				<pattern>/*.js</pattern>
				<pattern>/*.css</pattern>
			</excludes>
       </pre>
	 */
	private String[] excludes;
	private SitemeshDecoratorPath[] decorators;
	
	public String[] getExcludes() {
		return excludes;
	}
	public void setExcludes(String[] excludes) {
		this.excludes = excludes;
    }
	public SitemeshDecoratorPath[] getDecorators() {
		return decorators;
	}
	public void setDecorators(SitemeshDecoratorPath[] decorators) {
		this.decorators = decorators;
	}
}///~
