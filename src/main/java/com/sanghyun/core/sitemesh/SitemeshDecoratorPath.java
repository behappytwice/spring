package com.sanghyun.core.sitemesh;

public class SitemeshDecoratorPath {

	
	/**
	 * URL 패턴에서 데코레이터를 적용할 경로 
	 * <pre>
	 * /mng2/*
	 * </pre>
	 */
	private String contentPath;
	
	/**
	 * 데코레이터 파일의 경로 
	 * <pre>
	 * mng/orgMngDecorator.jsp"
	 * </pre>
	 */
	private String decoratorPath;
	
	public String getContentPath() {
		return contentPath;
	}
	public void setContentPath(String contentPath) {
		this.contentPath = contentPath;
	}
	public String getDecoratorPath() {
		return decoratorPath;
	}
	public void setDecoratorPath(String decoratorPath) {
		this.decoratorPath = decoratorPath;
	}
	
}//~
