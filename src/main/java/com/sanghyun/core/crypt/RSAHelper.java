package com.sanghyun.core.crypt;
/*
* Naonsoft Inc., Software License, Version 7.0
*
* Copyright (c) 2012 Naonsoft Inc.,
* All rights reserved.
*
* DON'T COPY OR REDISTRIBUTE THIS SOURCE CODE WITHOUT PERMISSION.
* THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL <<Naonsoft Inc.>> OR ITS
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
* USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
* OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
* SUCH DAMAGE.
*
* For more information on this product, please see
* <<www.naonsoft.com>>
*/

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * <p>
 * <b>공개키기반의 암호화를 지원합니다.</b>
 * </p>
 * 서버와 클라이언트간에 데이타를 주고받을 경우에 서버로 부터 공개키를 받아와서 공개키를 이용하여 데이타를 암호화할 키를 암호화하여 서버로
 * 전송합니다. 서버는 자신의 개인키를 이용하여 암호화에 사용된 세션키를 복호화하여 클라이언트로 부터 전송받은 데이타를 복호화 하여야 합니다.
 * <br>
 * 
 * <pre>
 * 
 * private byte[] encryptionKey = new byte[16];
 * 
 * {@code @Test}
 * public void testRSA() throws Exception {
 *       // 공개키가 저장된 파일명
 *       String publicKeyFile = "c:\\publickey.txt";
 *       // 개인키가 저장된 파일명
 *       String privateKeyFile = "c:\\privatekey.txt";
 * 
 * 
 *       // 키를 저장하는 것은 한번만 수행함.
 *       // 키쌍을 생성
 *       KeyPair keyPair = RSAHelper.createKeyPair();
 *       // 공개키를 디스크에 저장
 *       RSAHelper.saveKeyAsFile(keyPair.getPublic().getEncoded(), publicKeyFile);
 *       // 개인키를 디스크에 저장
 *       RSAHelper.saveKeyAsFile(keyPair.getPrivate().getEncoded(), privateKeyFile);
 * 
 * 
 *       // 디스크로 부터 공개키를 읽음
 *       byte[] publicBytes = RSAHelper.getKeyFromFile(publicKeyFile);
 *       byte[] privateBytes = RSAHelper.getKeyFromFile(privateKeyFile);
 * 
 *       // 클라이언트로 전송하고 암호화에 사용된 키를 받아온다.
 *       byte[] encryptedEncryptionKey = doEncryption(publicBytes);
 * 
 *       // 개인키를 구함
 *       PrivateKey privateKey = RSAHelper.getPrivateKey(privateBytes);
 *       // 클라이언트의 암호화에 사용된 키를 복호화
 *       byte[] _encryptionKey = RSAHelper.decodeKey(encryptedEncryptionKey, privateKey);
 * 
 *       System.out.println("length2=" + _encryptionKey.length);
 * 
 *       // 클라이언트에서 암호화된 데이타를 전송받음
 *       String encryptedData = getEncryptedData();
 * 
 *       System.out.println(AESHelper.decrypt(_encryptionKey, encryptedData));
 * 
 * }//:
 * 
 * 
 * 
 * // 서버로 부터 받은 공개키를 이용하여 암호화 된 키를 다시 서버로 전송
 * private byte[] doEncryption(byte[] publicKeyBytes) throws Exception {
 * 
 *       // PublicKey를 구하고
 *       PublicKey pk = RSAHelper.getPublicKey(publicKeyBytes);
 *       // 데이타 암호화에 사용할 키를 생성하고
 *       // byte[] encryptionKey = RSAHelper.createSymmetryKey();
 *       encryptionKey = RSAHelper.createSymmetryKey();
 *       // 암호화에 사용할 키를 암호화 하고 서버로 전송
 *       return RSAHelper.encodeKey(encryptionKey, pk);
 *  }//:
 * 
 * 
 * 
 *  // 클라이언트에서 암호화
 *  private String getEncryptedData() throws Exception {
 * 
 *       String plainText = "안녕하세요 아이온 장은정입니다. \n"
 *       + "제가 확인해본 바로는 가입설계번호는 VARCHAR2(18) 18자리 입니다.\n"
 *       + "18자리로 수정 해주셔야 할 것 같습니다. \n"
 *       + "작업이 완료되면 연락 부탁드리겠습니다. \n"
 *       + "제가 확인해본 바로는 가입설계번호는 VARCHAR2(18) 18자리 입니다.\n"
 *       + "18자리로 수정 해주셔야 할 것 같습니다. \n"
 *       + "작업이 완료되면 연락 부탁드리겠습니다. \n";
 * 
 *       return AESHelper.encrypt(encryptionKey, plainText);
 *  }//:
 * 
 * </pre>
 * 
 * @author 김상현(sanghyun@naonsoft.com)
 * @author leesooyong(megatoon@naonsoft.kr) 주석 추가
 * @version 7.0
 * @see <a href=
 *      "https://ko.wikipedia.org/wiki/RSA_%EC%95%94%ED%98%B8">https://ko.wikipedia.org/wiki/RSA_%EC%95%94%ED%98%B8</a>
 */
public class RSAHelper {

	/**
	 * 생성자
	 */
	public RSAHelper() {
	}

	/**
	 * 공개키/개인키 KeyPair을 생성합니다.
	 * 
	 * @return 생성된 키쌍
	 * @throws Exception
	 *             the Exception
	 * @see <a href=
	 *      "https://docs.oracle.com/javase/7/docs/api/java/security/KeyPairGenerator.html">https://docs.oracle.com/javase/7/docs/api/java/security/KeyPairGenerator.html</a>
	 */
	public static KeyPair createKeyPair() throws Exception {
		KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
		keyPairGen.initialize(2048);
		return keyPairGen.genKeyPair();
	}// :

	
	
	/**
	 * 공개키의 byte[]로 공개키를 생성합니다.
	 * 
	 * @param keyBytes
	 *            공개키의 바이트배열
	 * @return 공개키
	 * @throws Exception
	 *             the Exception
	 *
	 */
	public static PublicKey getPublicKey(byte[] keyBytes) throws Exception {
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicKey = keyFactory.generatePublic(keySpec);
		return publicKey;
	}// :

	/**
	 * 개인키의 byte[]로 개인키를 생성합니다.
	 * 
	 * @param keyBytes
	 *            개인키의 바이트배열
	 * @return 개인키
	 * @throws Exception
	 *             the Exception
	 */
	public static PrivateKey getPrivateKey(byte[] keyBytes) throws Exception {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
		return privateKey;
	}// :

	/**
	 * 암호화 하는데 사용할 대칭키 생성.
	 * 
	 * @return 세션에서 사용할 암호화키의 바이트 배열
	 * @throws Exception
	 *             the Exception
	 */
	public static byte[] createSymmetryKey() throws Exception {
		// 암호화 하는 데 쓸 대칭키 생성
		SecretKey key = null;
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		kgen.init(128);
		key = kgen.generateKey();
		byte[] bytes = new byte[16];
		bytes = key.getEncoded();
		// System.out.println("length=" + bytes.length);
		return bytes;
	}// :

	/**
	 * 공개키로 대칭키알고리즘으로 생성한 비밀키를암호화 한다.
	 * 
	 * @param keyBytes
	 *            비밀키의 바이트 배열
	 * @param publicKey
	 *            공개키
	 * @return 암호화된 비밀키
	 * @throws Exception
	 *             the Exception
	 */
	public static byte[] encodeKey(byte[] keyBytes, PublicKey publicKey) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		// Perform the actual encryption on those bytes
		byte[] cipherText = cipher.doFinal(keyBytes);
		return cipherText;
	}// :

	/**
	 * 개인키로 암호화된 비밀키를 복호화 한다.
	 * 
	 * @param encrypted
	 *            암호화된 비밀키의 바이트 배열
	 * @param privateKey
	 *            비밀키
	 * @return 복호화된 비밀키
	 * @throws Exception
	 *             the Exception
	 */
	public static byte[] decodeKey(byte[] encrypted, PrivateKey privateKey) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return cipher.doFinal(encrypted);
	}// :
	
	public static String decryptRsa(PrivateKey privateKey, String securedValue) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA");
		byte[] encryptedBytes = hexToByteArray(securedValue);
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
		String decryptedValue = new String(decryptedBytes, "utf-8"); // 문자 인코딩 주의.
		return decryptedValue;
	}
	

	public static byte[] hexToByteArray(String hex) {
		if (hex == null || hex.length() % 2 != 0) {
			return new byte[] {};
		}

		byte[] bytes = new byte[hex.length() / 2];
		for (int i = 0; i < hex.length(); i += 2) {
			byte value = (byte) Integer.parseInt(hex.substring(i, i + 2), 16);
			bytes[(int) Math.floor(i / 2)] = value;
		}
		return bytes;
	}

	/**
	 * 공개키/개인키를 파일로 저장.
	 *
	 * @param keyBytes
	 *            키의 바이트배열
	 * @param filePath
	 *            저장할 파일명(풀패스)
	 * @throws Exception
	 *             the Exception
	 */
	public static void saveKeyAsFile(byte[] keyBytes, String filePath) throws Exception {
		// 파일 시스템에 암호화된 공개키를 쓴다.
		FileOutputStream fos = new FileOutputStream(filePath);
		fos.write(keyBytes);
		fos.close();
	}// :

	/**
	 * 공개키/개인키를 파일로 부터 읽어들인다.
	 * 
	 * @param filePath
	 *            저장된 파일명(풀패스)
	 * @return 키의 바이트배열
	 * @throws Exception
	 *             the Exception
	 */
	public static byte[] getKeyFromFile(String filePath) throws Exception {

		FileInputStream fis = new FileInputStream(filePath);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int theByte = 0;
		while ((theByte = fis.read()) != -1) {
			baos.write(theByte);
		}
		fis.close();
		byte[] keyBytes = baos.toByteArray();
		baos.close();

		return keyBytes;
	}// :

	/**
	 * Modulus, Exponent 값을 이용하여 PrivateKey 객체를 생성함
	 * 
	 * @param modulus
	 * @param privateExponent
	 * @return
	 */
	public static PrivateKey getPrivateKey(String modulus, String privateExponent) {
		BigInteger modulus_ = new BigInteger(modulus);
		BigInteger privateExponent_ = new BigInteger(privateExponent);
		PrivateKey privateKey = null;
		try {
			privateKey = KeyFactory.getInstance("RSA")
					.generatePrivate(new RSAPrivateKeySpec(modulus_, privateExponent_));
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return privateKey;
	}

	/**
	 * Moduls, Exponent 값을 이용하여 PublicKey 객체를 생성함
	 * 
	 * @param modulus
	 * @param exponent
	 * @return
	 */
	public static PublicKey getPublicKey(String modulus, String exponent) {
		BigInteger modulus_ = new BigInteger(modulus);
		BigInteger exponent_ = new BigInteger(exponent);
		PublicKey publicKey = null;
		try {
			publicKey = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(modulus_, exponent_));
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return publicKey;
	}

	/**
	 * RSA 비밀키로부터 RSAPrivateKeySpec 객체를 생성함
	 * 
	 * @param privateKey
	 * @return
	 */
	public static RSAPrivateKeySpec getRSAPrivateKeySpec(PrivateKey privateKey) {
		RSAPrivateKeySpec spec = null;
		try {
			spec = KeyFactory.getInstance("RSA").getKeySpec(privateKey, RSAPrivateKeySpec.class);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return spec;
	}

	/**
	 * PublicKey로부터 RSAPublicKeySpec을 생성한다.
	 * 
	 * @param publicKey
	 * @return
	 */
	public static RSAPublicKeySpec getRSAPublicKeySpec(PublicKey publicKey) {
		RSAPublicKeySpec spec = null;
		try {
			spec = KeyFactory.getInstance("RSA").getKeySpec(publicKey, RSAPublicKeySpec.class);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return spec;
	}

}/// ~
