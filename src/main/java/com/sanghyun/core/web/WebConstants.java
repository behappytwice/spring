package com.sanghyun.core.web;

/**
 * WEB 통신시 사용할 상수값으로 오류코드를 포함한다.  
 * @author Andy
 *
 */
public class WebConstants {
	
	/** 기본 ENCODING */
	public static final String DEFAULT_ENCODING = "utf-8";
	/**  성공 코드 */
	public static final String DEFAULT_CODE_SUCCESS = "0";
	/** 시스템 오류 코드 */ 
	public static final String DEFAULT_CODE_FAILURE = "500";
	/** 접근이 허용되지 않는 경우에 돌리는 코드 */ 
	public static final String DEFAULT_CODE_ACCESS_DENIED = "900"; 
	/** 파일이 존재하지 않으면 이동할 화면 */
	public static final String DEFAULT_NOT_FOUND_VIEW = "error/notFound";
	/** 시스템 오류가 발생하면 이동할 화면 */
	public static final String DEFAULT_INTERNAL_SERVER_ERROR_VIEW = "error/internalError";
	/** AJAX 통신 시 사용할 HEADER 값 */
	public static final String HTTP_XHR_HEADER_REQUESTER_NAME = "X-XHR-REQUESTER-NAME";
	/** AJAX 통신 시 사용할 HEADER 값 */
	public static final String HTTP_XHR_HEADER_USE_WRAPPER_CLASS= "X-XHR-RETURN-TYPE";
	
}///~
