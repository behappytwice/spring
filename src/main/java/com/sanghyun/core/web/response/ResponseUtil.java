package com.sanghyun.core.web.response;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sanghyun.core.util.JSONUtils;
import com.sanghyun.core.web.WebConstants;

public class ResponseUtil {

    
    /**
	 * 문자열을 파라미터로 받은 charset으로 응답을 보낸다.<br><br>
	 * 사용 예제 : <br>
	 * <pre>
	 * 		ResponseHelper.printToClient(response, "응답", "utf-8");
	 * </pre>
     * @param response 웹응답
     * @param data 응답데이타
     * @param charset 문자셋
     */
    public  void writeToClient(HttpServletResponse response, String data, String charset) {
    	response.setCharacterEncoding(charset);
        PrintWriter out = null;
        try {
            out = new PrintWriter(response.getWriter());
            out.print(data);
            out.flush();
        } catch (Exception e) {
            response.setStatus(500); // internal server error
            e.printStackTrace(System.out);
        } finally {
            if (out != null)
                out.close();
        }
    }//:

    
    /**
     * Converts an exception to a string
     * @param t
     * @return
     */
    public String convertExceptionToString(Throwable t) {
    	 ByteArrayOutputStream bos = new ByteArrayOutputStream();
         PrintStream ps = new PrintStream(bos);
         t.printStackTrace(ps);
         return bos.toString();
    }//:
       
    public void printOutError(HttpServletRequest request, HttpServletResponse response, Throwable t, String charset) {
    	
    	charset = (charset == null) ? WebConstants.DEFAULT_ENCODING : charset;
    	response.setContentType("application/json; charset=" + charset);
    	ResponseMessage message = new ResponseMessage();
    	message.setCode(WebConstants.DEFAULT_CODE_FAILURE);
    	message.setError(convertExceptionToString(t));
    	
    	//ObjectMapper objectMapper = new ObjectMapper();
    	try { 
    		String convertedMessage = JSONUtils.writeValueAsString(message);
    		System.out.println(convertedMessage); 
    		writeToClient(response, convertedMessage, charset); 
    	}catch(Exception e) { 
    		throw new RuntimeException(e); 
    	}
    	

		//SampleJsonEmployeeBean emp = objectMapper.readValue(new File("employee.json"), SampleJsonEmployeeBean.class);
		
    	
    }//
    
    
}///~
