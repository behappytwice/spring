package com.sanghyun.core.web.response;


@Deprecated
public class ResponseData {
	
	/** 세션아이디를 보관한다. */ 
	private String sessionId;  
	/** 응답 코드 : 정상인 경우 0을 되돌린다. */ 
	private String responseCode;
	/** 응답메시지 : 정상인 경우 '정상적으로 처리되었습니다.'를 되돌린다. */ 
	private String responseText; 
	/** 시스템 오류인 경우 시스템 오류를 되돌린다. */ 
	private String systemError;
	/** 응답 데이타. */ 
	private Object data;

	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseText() {
		return responseText;
	}
	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	
	
	
}///~
