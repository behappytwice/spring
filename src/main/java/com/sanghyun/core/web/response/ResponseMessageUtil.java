package com.sanghyun.core.web.response;

import com.sanghyun.core.web.WebConstants;

public class ResponseMessageUtil {
	
	
	public static ResponseMessage create(Object data) { 
		ResponseMessage message = new ResponseMessage();
    	message.setCode(WebConstants.DEFAULT_CODE_SUCCESS);
    	message.setError("");
    	message.setData(data);
    	return message; 
		
	}//:
	
	
}
