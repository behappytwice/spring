package com.sanghyun.core.web.error;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sanghyun.core.web.WebConstants;


///**
// * web.xml에서  notfound, internalError을 등록하면 이 컨트롤에서 URL을 처리한다.
// * @author Andy
// *
// */

/**
 *
 * <error-page>
<error-code>404</error-code>
<!-- <exception-type>java.lang.ArithmeticException</exception-type> -->
<location>/notFound</location>
</error-page>

<error-page>
<error-code>500</error-code>
<!-- <exception-type>java.lang.ArithmeticException</exception-type> -->
<location>/internalError</location>
</error-page>
 */

@Controller
public class ContainerErrorHandleController  extends WebConstants {
	
	@RequestMapping("/notFound")
	public String handleNotFound() throws Exception {
		System.out.println("@@@@@@@@@@@@ File Not Found Error");
		return DEFAULT_NOT_FOUND_VIEW;
	}
	
	@RequestMapping("/internalError")
	public String handleInternalError() throws Exception {
		
		System.out.println("@@@@@@@@@@@@@ INTERNAL SERVER ERROR");

		return DEFAULT_INTERNAL_SERVER_ERROR_VIEW;
	}
}///~
