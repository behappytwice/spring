package com.sanghyun.core.web.error;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.sanghyun.core.web.WebConstants;
import com.sanghyun.core.web.response.ResponseUtil;

@ControllerAdvice
public class GlobalDefaultExceptionHandler extends WebConstants {

	
//	@ResponseStatus(HttpStatus.NOT_FOUND)
//	@ExceptionHandler(NoHandlerFoundException.class) 
//	public ModelAndView handleError404(HttpServletRequest req, HttpServletResponse res, Exception ex) throws Exception {
//		return new ModelAndView(DEFAULT_NOT_FOUND_VIEW); 
//	}//:

	 @ExceptionHandler(NoHandlerFoundException.class)
     public String handle(Exception ex) {
		 System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>> 404 not found.");
		 return DEFAULT_NOT_FOUND_VIEW;
	 }
	 
//	@ExceptionHandler(Exception.class)
//	public ModelAndView exception(Exception e) { 
//		return new ModelAndView(DEFAULT_INTERNAL_SERVER_ERROR_VIEW);
//	}//:
	
	
//	@ExceptionHandler({SQLException.class,DataAccessException.class})
//    public String databaseError() {
//		// not implemented
//		return null;
//	}//:
	
	@ExceptionHandler(Exception.class)
	public void errorHandlerWithServiceRequest(HttpServletRequest request, HttpServletResponse response,  Exception e) throws IOException  {
		
		String reqeusterName = request.getHeader(WebConstants.HTTP_XHR_HEADER_REQUESTER_NAME);
		//res.sendRedirect(req.getServletContext() + "/internalError");
		if(reqeusterName != null) {
			new ResponseUtil().printOutError(request, response, e, null);
		}else { 
			throw new RuntimeException(e);
			//return DEFAULT_INTERNAL_SERVER_ERROR_VIEW;
		}
	}//:
	
	
	// MailException.class
	// 
	
}///~
