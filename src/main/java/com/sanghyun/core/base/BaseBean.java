package com.sanghyun.core.base;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.sanghyun.core.log.Log;



/**
 * 모든 클래스의 부모 클래스이다.
 * @author Andy
 *
 */
@Component
public class BaseBean {
	protected static @Log Logger LOG;
	@Autowired
	protected MessageSource messageSource; 
	@Autowired
	protected  ApplicationContext applicationContext;
	@Resource
	protected Environment env;
	
}///~
