package com.sanghyun.core.base.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * URI 정보가 없이 호출될 때 기본으로 index.jsp를 호출하도록 설정한 Controller 
 * @author Andy
 *
 */
@Controller
public class NavigationController  {

	/**
	 * URI 정보가 없이 호출될 때 기본으로 index.jsp를 호출한다. 
	 * @return
	 */
	@RequestMapping(value={"/", "index"}, method=RequestMethod.GET)
	public ModelAndView index() {
		return new ModelAndView("index2");
	}
	
}
