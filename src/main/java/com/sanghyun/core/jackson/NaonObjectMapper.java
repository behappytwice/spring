package com.sanghyun.core.jackson;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class NaonObjectMapper extends ObjectMapper {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new custom object mapper.
	 */
	public NaonObjectMapper(List<String> dateFormat) {
		SimpleModule simpleModule = new SimpleModule();
		//simpleModule.addSerializer(java.util.Date.class, new DateSerializer(dateFormat));

		simpleModule.addDeserializer(java.util.Date.class, new JsonDateDeserializer(dateFormat));
		registerModule(simpleModule);
	}
}
