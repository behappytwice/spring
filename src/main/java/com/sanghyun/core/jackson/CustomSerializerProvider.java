package com.sanghyun.core.jackson;


import java.util.List;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;


/**
 * @RequstBody, @ResponseBody 처리를 위한  ObjectMapper를  위한 Serializer Provider입니다.
 * @author Sanghyun,Kim(sanghyun@naonsoft.com)
 *
 */
public class CustomSerializerProvider extends DefaultSerializerProvider {

	/**
	 * Constructor
	 */
    public CustomSerializerProvider() {
        super();
    }

    /**
     * Constructor
     * @param provider
     * @param config
     * @param jsf
     */
    public CustomSerializerProvider(CustomSerializerProvider provider, SerializationConfig config,
            SerializerFactory jsf) {
        super(provider, config, jsf);
    }

    @Override
    public CustomSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
        return new CustomSerializerProvider(this, config, jsf);
    }

    /**
     * 
     */
    @Override
    public JsonSerializer<Object> findNullValueSerializer(BeanProperty property) throws JsonMappingException {
    	
        if (property.getType().getRawClass().equals(String.class)) {
            return StringSerializer.EMPTY_STRING_SERIALIZER_INSTANCE;
        }else if (property.getType().getRawClass().equals(List.class)) {
            return ListSerializer.EMPTY_LIST_SERIALIZER_INSTANCE;
        }
        else { 
            return super.findNullValueSerializer(property);
        }
    }
}