package com.sanghyun.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.env.Environment;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sanghyun.core.appconfig.SringArrayForJSON;
import com.sanghyun.core.beans.ObjectArrayForJSON;

public class AppConfigUtil  {
	
	
	public static boolean isProfileExists(Environment env, String profile) {
		if(Arrays.stream(env.getActiveProfiles()).anyMatch(  envl -> (  envl.equalsIgnoreCase(profile) ))) 
		{
			return true;
		}
		return false; 
	}
	
	public static  List<String> readJSON(String path) throws Exception {
		//
		List<String> list = new ArrayList<String>();
		ObjectMapper objectMapper = new ObjectMapper();
		SringArrayForJSON i18bean = objectMapper.readValue(ClassPathFileUtil.getFileObject(path), SringArrayForJSON.class);
		if(i18bean.getItems() != null) {
			for(String ifile : i18bean.getItems()) { 
				list.add(ifile.trim());
			}
		}
		return list; 
	}//:
	
	public static <T>  ObjectArrayForJSON<T> readJSON(String path, Class<T> t ) throws Exception {
		try { 
			ObjectMapper objectMapper = new ObjectMapper();
//			JavaType jType = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, t);
//			TypeReference tr = new TypeReference<ObjectArrayForJSON<T>>() {};
			ObjectArrayForJSON<T> i18bean = objectMapper.readValue(ClassPathFileUtil.getFileObject(path), new ObjectArrayForJSON<T>().getClass());
			//ObjectArrayForJSON<T> i18bean = objectMapper.readValue(ClassPathFileUtil.getFileObject(path), ObjectArrayForJSON<jType>.class);
			return i18bean;
		}catch(Exception e) {
			// Ignores exception 
			//e.printStackTrace();
			//return null;
			return new ObjectArrayForJSON<T>();
		}
	}//:
		
	
	public static <T>  List<T> readJSONToList(String path, Class<T> t ) throws Exception {
		try { 
			ObjectMapper objectMapper = new ObjectMapper();
			JavaType jType = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, t);
//			TypeReference tr = new TypeReference<ObjectArrayForJSON<T>>() {};
			List<T> i18bean = objectMapper.readValue(ClassPathFileUtil.getFileObject(path), jType);
			return i18bean;
		}catch(Exception e) {
			// Ignores exception 
			//e.printStackTrace();
			return new ArrayList<T>();
		}
	}//:
	
}
