package com.sanghyun.core.util;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXB;

import com.thoughtworks.xstream.XStream;

public class XMLUtil {

	public static String marshal(Class clazz, Object o) { 
		StringWriter sw = new StringWriter();
		JAXB.marshal(o, sw);
		return sw.toString();
	}//:
	
	public static Object unmarshal(String xml, Class clazz) {
		return JAXB.unmarshal(new StringReader(xml), clazz);
	}//:
	
	public static String toXML(String alias, Class clazz, Object o) {
		XStream xstream = new XStream();
		xstream.alias("employee", clazz);
		return  xstream.toXML(o);
	}//:
	
	public static Object readXmlToObjectTest(String alias, Class clazz, String xml) {
		XStream xstream = new XStream();
		xstream.alias(alias, clazz);
		return xstream.fromXML(xml);
	}//:
	
}///~
