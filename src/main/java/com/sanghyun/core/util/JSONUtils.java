package com.sanghyun.core.util;

import java.io.File;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sanghyun.core.jackson.CustomSerializerProvider;
import com.sanghyun.core.jackson.NaonObjectMapper;


public class JSONUtils {

	//http://www.baeldung.com/jackson-custom-serialization
	//http://www.novixys.com/blog/jackson-json-serialization/
	
	public static String writeValueAsString(Object o) {
		try { 
	
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(o);
		}catch(Exception e) {
			throw new RuntimeException(e);
			
		}
	}//:
	public static void writeValue(File file, Object o) {
		try { 
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(file,o);
		}catch(Exception e) {
			throw new RuntimeException(e);
			
		}
	}//:
	
	public static Object readValue(File file, Class clazz) {
		try { 
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(file, clazz);
		}catch(Exception e) {
			throw new RuntimeException(e);
			
		}
	}//:
	

	public static Object readValue(String jsonString, Class clazz) {
		try { 
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(jsonString, clazz);
		}catch(Exception e) {
			throw new RuntimeException(e);
			
		}
	}//:
	
	/**
	 * NaonObjectMapper를 생성합니다.
	 * @param dateFormat SimpleDateFormat의 format. 지정할 필요가 없으면 null을 넘긴다.  
	 * @return ObjectMapper Instance
	 * 
	 */
	private static ObjectMapper getObjectMapper(String dateFormat) {
		NaonObjectMapper mapper = new NaonObjectMapper(null);
		// SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		// mapper.setDateFormat(df);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setSerializerProvider(new CustomSerializerProvider());
		if(dateFormat != null && dateFormat.length() >0) {
			SimpleDateFormat df = new SimpleDateFormat(dateFormat);
			mapper.setDateFormat(df);
		}
		// serializer = object to json
		// deserializer = json to object
		return mapper;
	}	
	
	
	
	
	
	
}///~
