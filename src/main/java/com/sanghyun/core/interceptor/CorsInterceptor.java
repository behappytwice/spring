package com.sanghyun.core.interceptor;

import java.lang.annotation.Annotation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sanghyun.core.annotation.AccessControlAllowOrigin;
import com.sanghyun.core.annotation.InterceptorAnnotation;
import com.sanghyun.core.log.Log;
import com.sanghyun.core.sitemesh.SitemeshFilter;
import com.sanghyun.core.util.HTTPUtil;

@InterceptorAnnotation(addPathPattern = { "/**" }, excludePathPattern = {})
@AccessControlAllowOrigin(allowOrigin = { "*", "http://localhost" })
public class CorsInterceptor extends HandlerInterceptorAdapter {
	
	final static Logger LOG = LoggerFactory.getLogger(SitemeshFilter.class);
	
	protected String[] allowOrigin;
	
	public CorsInterceptor() {
		try {
			Annotation[] annotations = this.getClass().getAnnotations();
			for (Annotation annotation : annotations) {
				if (annotation instanceof AccessControlAllowOrigin) {
					AccessControlAllowOrigin allow = (AccessControlAllowOrigin) annotation;
					this.allowOrigin = allow.allowOrigin();
					break;
				}
			} // for
		} catch (Exception e) {
			// Ignores this exception
			// TODO : display this error log
		}
	}//:
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

	
		LOG.info("======= [ Cross-Origin Resource Sharing(CORS) applied for " + HTTPUtil.getFullURL(request) + "]");
		LOG.info("■■■■■■  Origin:" + request.getHeader("Origin"));
		
		
		if(allowOrigin != null && allowOrigin.length == 1 && allowOrigin[0].equals("*")) {
			response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
			response.setHeader("Access-Control-Max-Age", "3600");
			response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		
			response.setHeader("Access-Control-Allow-Origin", "*");
		}else { 
			if(allowOrigin != null && allowOrigin.length > 0) {
				for(String allowUrl : allowOrigin) {
					if( allowUrl.equals(request.getHeader("Origin")))  {
						response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
						response.setHeader("Access-Control-Max-Age", "3600");
						response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
					
						response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
						return true;
					}
				}
			}
		}
		return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

}
