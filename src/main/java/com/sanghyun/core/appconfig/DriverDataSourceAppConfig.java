package com.sanghyun.core.appconfig;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.sanghyun.core.log.Log;
import com.sanghyun.core.util.AppConfigUtil;

import net.sf.log4jdbc.Log4jdbcProxyDataSource;
import net.sf.log4jdbc.tools.Log4JdbcCustomFormatter;
import net.sf.log4jdbc.tools.LoggingType;

@Configuration
public class DriverDataSourceAppConfig {
	
	@Resource
	private Environment env;
	protected static @Log Logger LOG;
	
	private DataSource driverManagerDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
		if(AppConfigUtil.isProfileExists(env, AppConfigConstants.PROPERTY_PROFILE_DEV)) { 
			dataSource.setDriverClassName(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_DEV_DATABASE_DRIVER));
			dataSource.setUrl(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_DEV_DATABASE_URL));
			dataSource.setUsername(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_DEV_DATABASE_USERNAME));
			dataSource.setPassword(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_DEV_DATABASE_PASSWORD));
			
		}else if(AppConfigUtil.isProfileExists(env, AppConfigConstants.PROPERTY_PROFILE_STG)) {
			dataSource.setDriverClassName(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_STG_DATABASE_DRIVER));
			dataSource.setUrl(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_STG_DATABASE_URL));
			dataSource.setUsername(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_STG_DATABASE_USERNAME));
			dataSource.setPassword(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_STG_DATABASE_PASSWORD));
			
		}else {
			dataSource.setDriverClassName(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_PROD_DATABASE_DRIVER));
			dataSource.setUrl(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_PROD_DATABASE_URL));
			dataSource.setUsername(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_PROD_DATABASE_USERNAME));
			dataSource.setPassword(env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_PROD_DATABASE_PASSWORD));
		}
		
		return dataSource;
	}

	@Bean
	/**
	 * SQL Logging을 위한 DataSource
	 * @return
	 */
	public DataSource dataSource() {
		
		Log4JdbcCustomFormatter formatter = new Log4JdbcCustomFormatter();
		//formatter.setLoggingType(LoggingType.MULTI_LINE);
		formatter.setLoggingType(LoggingType.SINGLE_LINE);
		formatter.setSqlPrefix("QUERY:-------------------------------------------------------\r");

		//diObjectFactoryBean bean = jndiObjectFactoryBean();
		//Log4jdbcProxyDataSource dataSource = new Log4jdbcProxyDataSource((DataSource) jndiObjectFactoryBean());
		Log4jdbcProxyDataSource _dataSource = new Log4jdbcProxyDataSource(driverManagerDataSource());
		_dataSource.setLogFormatter(formatter);
		
		return _dataSource; 
	}// :
	
}
