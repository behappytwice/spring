package com.sanghyun.core.appconfig;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.sanghyun.core.log.Log;
import com.sanghyun.core.util.AppConfigUtil;

@Configuration
public class MyBatisAppConfig {
	
	@Autowired
	//@Qualifier("dataSource")
	DataSource dataSource; 
	protected static @Log Logger LOG;
	@Resource
	private Environment env;
	
	/**
	 * Creates TransactionManager object.
	 * @return
	 */
	@Bean
	@Qualifier("dataSourceTransactionManager")
	public DataSourceTransactionManager dataSourceTransactionManager() {
		return new DataSourceTransactionManager(dataSource);
	}//:

	/**
	 * Creates SqlSessionFactoryBean instance. It is used by SqlSessionTemplate.
	 * 
	 * @return SqlSessionFactory
	 * @throws Exception
	 */
	private SqlSessionFactory sqlSessionFactory() throws Exception {
		
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(dataSource); 
		sqlSessionFactory.setTypeAliasesPackage( AppConfigConstants.CONST_BASE_DODMAIN_PATH);
		
		// <property name="mapperLocations"
		// value="classpath*:com/naon/biz/demo/sqlmap/${dbtypeLowerCase}/**/*.xml" />
		// org.springframework.core.io.Resource mapperResource = new
		// ClassPathResource("/sqlmap/oracle/*EmployeeMapper.xml"); // db type 별로 분리 가능
		// sqlSessionFactory.setMapperLocations(new
		// org.springframework.core.io.Resource[] { mapperResource });

//		ClassLoader cl = this.getClass().getClassLoader();
//		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(cl);
//		// TODO : mapper를 dbtype별로 읽을 수 있도록 수정해야 함 
//		org.springframework.core.io.Resource[] mapperResource = resolver
//				.getResources("classpath*:com/sanghyun/sqlmap/biz/**/oracle/**/*Mapper.xml");
		
		sqlSessionFactory.setMapperLocations(getMapperResource());
		return (SqlSessionFactory) sqlSessionFactory.getObject();
	}

	// <bean id="sqlSessionTemplate" class="org.mybatis.spring.SqlSessionTemplate">
	// <qualifier value="mainDB" />
	// <constructor-arg index="0" ref="sqlSession" />
	// </bean>
	
	private org.springframework.core.io.Resource[] getMapperResource() throws Exception {
		
		String dbType = env.getProperty(AppConfigConstants.PROPERY_NAME_DB_TYPE); // db.type 
		ClassLoader cl = this.getClass().getClassLoader();
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(cl);
		List<org.springframework.core.io.Resource> mapperList = new ArrayList<org.springframework.core.io.Resource>();
		org.springframework.core.io.Resource[] mapperResource = null;
//		org.springframework.core.io.Resource[] mapperResource = resolver.getResources("classpath*:com/sanghyun/sample/sqlmap/**/" + dbType.trim() + "/*Mapper.xml");
//		for(org.springframework.core.io.Resource res : mapperResource) {
//			mapperList.add(res); 
//		}
		mapperResource = resolver.getResources("classpath*:com/sanghyun/sqlmap/biz/**/" + dbType.trim() + "/*Mapper.xml");
		for(org.springframework.core.io.Resource res : mapperResource) {
			mapperList.add(res); 
		}
		
		try {
			List<String> list = AppConfigUtil.readJSON("mapper-extension.json"); 		
			for(String extPath : list) {
				org.springframework.core.io.Resource[] mapres = resolver.getResources(extPath.trim());
				for(org.springframework.core.io.Resource res : mapres) {
					mapperList.add(res); 
				}
			}
			
		}catch(Exception e) { 
			// Ignores this error
		}
	
		org.springframework.core.io.Resource[] resourceToReturn = new org.springframework.core.io.Resource[mapperList.size()];
		resourceToReturn = mapperList.toArray(resourceToReturn);
		return resourceToReturn;
	}//:
	
	
	/**
	 * Creates SqlSessionTempalte instance. This requires SqlSessionFactory object.
	 * 
	 * @return SqlSessionTemplate object
	 * @throws Exception
	 */
	@Bean
	public SqlSession sqlSessionTemplate() throws Exception {
		SqlSessionTemplate sqlsession = new SqlSessionTemplate(sqlSessionFactory());
		return sqlsession;
	}
}///~
