package com.sanghyun.core.appconfig;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

import com.sanghyun.core.log.Log;

@Configuration
public class JNDIDataSourceAppConfig {

	@Resource
	protected Environment env;
	protected static @Log Logger LOG;
	
	@Bean
	//@Qualifier("jpaDataSource")
	public DataSource dataSource() throws NamingException {
		String jndiName = env.getProperty( AppConfigConstants.PROPERY_NAME_JNDI_NAME ); // jndi.name
//		return (DataSource) new JndiTemplate().lookup("java:/comp/env/" + jndiName.trim());
		
		final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
        dsLookup.setResourceRef(true);
        DataSource dataSource = dsLookup.getDataSource(jndiName.trim());
        return dataSource;		
	}
	
	//
//	@Bean
//	@Qualifier("dataSource")
//	/**
//	 * SQL Logging을 위한 JNDIFACTORYBEAN
//	 * 
//	 * 
//	 * 
//	 * 
//	 * @return
//	 */
//	public JndiObjectFactoryBean jndiObjectFactoryBean() {
//		String jndiName = env.getRequiredProperty(SpringConfigUtil.PROPERY_NAME_JNDI_NAME);
//		JndiObjectFactoryBean jndiBean = new JndiObjectFactoryBean();
//		jndiBean.setJndiName(jndiName.trim());
//		jndiBean.setResourceRef(true);
//		try {
//			jndiBean.afterPropertiesSet();
//		} catch (Exception e) {
//			LOG.error(e.getMessage());
//			return null;
//		}
//		return jndiBean;
//	}// :
	
}
