package com.sanghyun.core.appconfig;

import java.util.Arrays;

import javax.annotation.Resource;

import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import com.sanghyun.core.log.Log;

@Configuration
@EnableJms
public class JMSConfiguration {
	
	@Resource
	protected Environment env;
	protected static @Log Logger LOG;
	

	//private static final String DEFAULT_BROKER_URL = "tcp://localhost:61616";
//	private static final String ORDER_QUEUE = "order-queue";

	@Bean
	public ActiveMQConnectionFactory connectionFactory() {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		connectionFactory.setBrokerURL(env.getProperty(AppConfigConstants.PROPERTY_NAME_ACTIVEMQ_BROKER_URL));
		
		connectionFactory.setTrustedPackages(Arrays.asList("com.websystique.springmvc"));
		return connectionFactory;
	}

	@Bean
	public CachingConnectionFactory cachingConnectionFactory() {
		return new CachingConnectionFactory(connectionFactory());
	}

	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory());
		factory.setMessageConverter(jacksonJmsMessageConverter());
		factory.setConcurrency("3-10");
		return factory;
	}

	@Bean
	public JmsTemplate jmsTemplate() {
		JmsTemplate template = new JmsTemplate();
		template.setConnectionFactory(connectionFactory());
		template.setMessageConverter(jacksonJmsMessageConverter());
		template.setDefaultDestinationName(env.getProperty(AppConfigConstants.PROPERTY_NAME_ACTIVEMQ_QUEUE_NAME));
		return template;
	}
	
	@Bean // Serialize message content to json using TextMessage
	public MessageConverter jacksonJmsMessageConverter() {
	    MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
	    converter.setTargetType(MessageType.TEXT);
	    converter.setTypeIdPropertyName("_type");
	    return converter;
	}
	
	
	
//	
//	@Bean 
//	public SampleJmsMessageSender sender() {
//		return new SampleJmsMessageSender();
//	}
//
//	@Bean
//	public SampleJmsMessageListener receiver() {
//		return new SampleJmsMessageListener();
//	}

}/// ~
