package com.sanghyun.core.appconfig;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Controller;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.sanghyun.core.log.Log;

import net.sf.log4jdbc.Log4jdbcProxyDataSource;
import net.sf.log4jdbc.tools.Log4JdbcCustomFormatter;
import net.sf.log4jdbc.tools.LoggingType;

/**
 * 
 * 
 * @see https://stackoverflow.com/questions/28823600/error-creating-bean-with-name-defaultservlethandlermapping
 * @see http://docs.jboss.org/hibernate/orm/3.6/reference/en-US/html/session-configuration.html
 * 
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "com.sanghyun"}
//     , includeFilters = { 
//        //@ComponentScan.Filter(type = FilterType.REGEX, pattern="com.test.External*")
//    	  @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value=com.test.ExternalComponentScan.class)
//       }
     , excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ANNOTATION, value = Controller.class),
		//@ComponentScan.Filter(type = FilterType.ANNOTATION, value = Configuration.class)
		//@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value=WebAppConfig.class)
		//@ComponentScan.Filter(type = FilterType.ASPECTJ, pattern="com.sanghyun..*WebAppConfig+")
		@ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.sanghyun.*.*JMSConfiguration+"),
		@ComponentScan.Filter(type = FilterType.REGEX, pattern="com.sanghyun.*.*WebAppConfig+"),
		@ComponentScan.Filter(type = FilterType.REGEX, pattern="com.sanghyun.*.JNDIDataSourceAppConfig+")
		}
)
//@PropertySources(
//{
//   @PropertySource("classpath:application.properties"),
//   @PropertySource(value = "classpath:application-site.properties", ignoreResourceNotFound=true), // if the property file not exists, the spring container will ignore "File Not Found Exceptin"
//   @PropertySource(value = "classpath:application-extend.properties", ignoreResourceNotFound=true) // if the property file not exists, the spring container will ignore "File Not Found Exceptin"
//})
// @MapperScan("sqlmap.oracle") // myBatis mapper.xml 파일 스캔 설정
//@PropertySource("classpath:application.properties")
//@EnableJpaRepositories("com.sanghyun")
//@Import( { com.naon.ExternalComponentScan.class })
public class JUNITAppConfig {

	
	
	@Resource
	private Environment env;
	protected static @Log Logger LOG;
	
	
}////~
