package com.sanghyun.core.appconfig;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.slf4j.Logger;
//import org.hibernate.ejb.HibernatePersistence;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.mvc.WebContentInterceptor;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sanghyun.core.annotation.InterceptorAnnotation;
import com.sanghyun.core.jackson.CustomSerializerProvider;
import com.sanghyun.core.jackson.NaonObjectMapper;
import com.sanghyun.core.log.Log;
import com.sanghyun.core.util.AppConfigUtil;

@Configuration
@EnableWebMvc
// @EnableTransactionManagement // Controller에서는 Transaction을 사용하지 않는다.
// @PropertySource("classpath:application.properties")
@ComponentScan(basePackages = { "com.sanghyun" }  // com.sanghyun 패키지 아래의 모든 컴포넌트를 스캔한다
// , includeFilters = {
// //@ComponentScan.Filter(type = FilterType.REGEX,
// pattern="com.test.External*")
// @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
// value=com.test.ExternalComponentScan.class)
// }
		, excludeFilters = { // 제외할 컴포넌트들을 설정한다
				// @ComponentScan.Filter(type = FilterType.ANNOTATION, value =
				// Controller.class),
				// @ComponentScan.Filter(type = FilterType.ANNOTATION, value =
				// Configuration.class)
				// @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
				// value=WebAppConfig.class)
				@ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.sanghyun.*.*JMSConfiguration+"),   // JMS 제외 
				@ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.sanghyun.*.*JunitAppConfig+"),  // JUnit 제외 
				@ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.sanghyun.*.*DriverDataSourceAppConfig+")  // DirverDataSourceApp* 제외 
		// @ComponentScan.Filter(type = FilterType.REGEX, pattern =
		// "com.sanghyun.*.*RootConfig+")
		})
// @PropertySources({ @PropertySource("classpath:application.properties"),
// @PropertySource(value = "classpath:application-site.properties",
// ignoreResourceNotFound = true),
// @PropertySource(value = "classpath:application-extend.properties",
// ignoreResourceNotFound = true)
// })
// )
// @MapperScan("sqlmap.oracle") // myBatis mapper.xml 파일 스캔 설정
// @PropertySource("classpath:application.properties")
// @EnableJpaRepositories("com.sanghyun")
// @Import( { com.naon.ExternalComponentScan.class })
// public class WebAppConfig extends WebMvcConfigurationSupport {
public class WebAppConfig extends WebMvcConfigurerAdapter {

	/*
	 * WebMvcConfigurerAdapter vs WebMvcConfigurationSupport
	 * 
	 * WebMvcConfigurationSupport 이것을 사용하려면 static resources를 사용할 때 고생할 각오를 해야 한다.
	 * 왜냐하면 작동을 하지 않는다. configureDefaultServletHandling()를 override해서 쓰면된다고 했지만 호출되지
	 * 않는다.
	 * 
	 * WebMvcConfigurerAdapter RequestMappingHandlerAdapter와 같은 Bean을 생성하면 오류가 난다.
	 * 
	 */
	@Resource
	private Environment env;

	protected static @Log Logger LOG;

//	@Override
//	public void configureMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
//		messageConverters.add(createXmlHttpMessageConverter());
//		messageConverters.add(new MappingJackson2HttpMessageConverter());
//		super.configureMessageConverters(messageConverters);
//	}
//	
//
//	private HttpMessageConverter<Object> createXmlHttpMessageConverter() {
//		MarshallingHttpMessageConverter xmlConverter = new MarshallingHttpMessageConverter();
//		XStreamMarshaller xstreamMarshaller = new XStreamMarshaller();
//		xmlConverter.setMarshaller(xstreamMarshaller);
//		xmlConverter.setUnmarshaller(xstreamMarshaller);
//
//		return xmlConverter;
//	}
	
	
	
	/**
	 * 리소스 핸들러들러를 등록한다. properties 파일에 jsVer 필드를 만들고 
	 * jsVer를 변경하면 다시 다운로드 받도록 한다.
	 */
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//
//		String jsVer = BasePropertyConstants.getInstance().getJsVer();
//		String jsVer = "1221";
		
//		registry.addResourceHandler("/resources" + jsVer + "/**").addResourceLocations("/resources/")
//				.setCachePeriod(-1);
		
//		// registry.addResourceHandler("/resources/biz/**/*");
//		// registry.addResourceHandler("/resources/component/**/*");
//		// registry.addResourceHandler("/resources/framework/**/*");
//		// registry.addResourceHandler("/resources/lib/**/*");
//	}
	
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");

	}// :
	
	
	/**
	 * WebContentInterceptor를 생성합니다.
	 * 
	 * @return
	 */
	@Bean
	public WebContentInterceptor webContentInterceptor() {
		WebContentInterceptor interceptor = new WebContentInterceptor();
		//interceptor.setCacheSeconds(-1);
		// https://kdevkr.github.io/archives/2018/understanding-cache-control/
		// cacheSeconds < 0 : 캐쉬 관련 설정을 하지 않음 
		// cacheSeconds = 0 : cache 방지
		//     Pragma: no-cache
		//     Expires: Thu, 01 Jan 1970 00:00:00 GMT
		//     Cache-Control: max-age=0, no-cache, no-store
		// cacheSeconds > 0 : cache 
		//    Expires: Wed, 03 Jul 2013 05:58:02 GMT
	    //    Cache-Control: max-age=31536000
		
	
		//CacheControl cacheControl = CacheControl.maxAge(1, TimeUnit.DAYS).noCache();
		interceptor.setCacheSeconds(-1);
		//interceptor.setCacheControl(cacheControl);
		
		//interceptor.setCacheSeconds(-1);
		//interceptor.setUseExpiresHeader(true);;
		// interceptor.setUseCacheControlHeader(true);
		// interceptor.setUseCacheControlNoStore(true);
		
		System.out.println("★★★★★★★★★★★★★★★ 캐쉬관련 설정을 하지 않음2");

		
		// setCacherMappings
		// default caching으로 부터 URL path를 제외하기 위하여 "-1"을 설정할 수 있음
		
		Properties cacheMapping = new Properties();
		String expires = "2592000";
		cacheMapping.put("/resources/biz/**/*", expires);
		cacheMapping.put("/resources/component/**/*", expires);
		cacheMapping.put("/resources/framework/**/*", expires);
		cacheMapping.put("/resources/lib/**/*", expires);
		interceptor.setCacheMappings(cacheMapping);
		return interceptor;
	}
	
	

	@Bean
	// MappingJacksonHttpMessageConverter converter() {
	MappingJackson2HttpMessageConverter converter() {
		// https://stackoverflow.com/questions/10650196/how-to-configure-mappingjacksonhttpmessageconverter-while-using-spring-annotatio1
		// MappingJacksonHttpMessageConverter converter = new
		// MappingJacksonHttpMessageConverter();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		// do your customizations here...
		return converter;
	}

	// @Bean
	// public RequestMappingHandlerAdapter requestMappingHandlerAdapter() {
	// RequestMappingHandlerAdapter handlerAdapter =
	// super.requestMappingHandlerAdapter();
	// // handlerAdapter.getMessageConverters().add(0,
	// // getProtobufJsonMessageConverter());
	// handlerAdapter.getMessageConverters().add(0, converter());
	// return handlerAdapter;
	// }

	@Bean
	public UrlBasedViewResolver setupViewResolver() {
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setOrder(2);
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}

	@Bean
	public FreeMarkerViewResolver freemarkerViewResolver() {
		FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
		resolver.setOrder(0);
		resolver.setCache(true);
		resolver.setPrefix("");
		resolver.setSuffix(".ftl");
		return resolver;
	}

	@Bean
	public FreeMarkerConfigurer freemarkerConfig() {
		FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
		freeMarkerConfigurer.setTemplateLoaderPath("/WEB-INF/views/");
		return freeMarkerConfigurer;
	}

	/** Container 기본 Handler를 true로 설정한다. */
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		// TODO Auto-generated method stub
		configurer.enable();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		LOG.info("My Aapplication Startup Log : Registering interceptors");

		// registry.addInterceptor(new
		// SampleInterceptor()).addPathPatterns("/sample/**");
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		try {
			List<String> list = new ArrayList<String>();
			try {
				List<String> ilist = AppConfigUtil.readJSON("interceptor.json");
				list.addAll(ilist);
			} catch (Exception e) {
				// Ignores this error
			}
			try {
				List<String> ilist = AppConfigUtil.readJSON("interceptor-extension.json");
				list.addAll(ilist);
			} catch (Exception e) {
				// Ignores this error
			}

			// if the size of the list is zero, there is nothing to do.
			if (list == null || list.size() == 0)
				return;

			String[] interceptorList = new String[list.size()];
			interceptorList = list.toArray(interceptorList);
			ClassLoader classLoader = this.getClass().getClassLoader();
			for (String className : list) {
				try {
					Class aClass = classLoader.loadClass(className.trim());
					Annotation[] annotations = aClass.getAnnotations();
					for (Annotation annotation : annotations) {
						if (annotation instanceof InterceptorAnnotation) {
							InterceptorAnnotation intercAnno = (InterceptorAnnotation) annotation;
							String[] addPathPattern = intercAnno.addPathPattern();
							String[] excludePathPattern = intercAnno.excludePathPattern();
							// registry.addInterceptor(new
							// SampleInterceptor()).addPathPatterns("/sample/**");
							InterceptorRegistration regis = registry
									.addInterceptor((HandlerInterceptor) aClass.newInstance());
							if (addPathPattern != null && addPathPattern.length > 0) {
								for (String pattern : addPathPattern) {
									regis.addPathPatterns(pattern);
								}
							}
							if (excludePathPattern != null && excludePathPattern.length > 0) {
								for (String pattern : excludePathPattern) {
									regis.excludePathPatterns(pattern);
								}
							}
						}
					} // for

				} catch (Exception e) {
					// Ignores this exception
					// TODO : display this error log
				}
			} // for
		} catch (Exception e) {
			// Ignores exceptions
		}

		LOG.info("My Aapplication Startup Log : Registered interceptors");
	}// :

//	@Bean
//	public Filter characterEncodingFilter() {
//		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
//		characterEncodingFilter.setEncoding("UTF-8");
//		characterEncodingFilter.setForceEncoding(true);
//		return characterEncodingFilter;
//	}

	@Bean
	public CookieLocaleResolver localeResolver() {
		CookieLocaleResolver localeResolver = new CookieLocaleResolver();
		localeResolver.setDefaultLocale(Locale.KOREA); // 기본 한국어
		localeResolver.setCookieName("locale"); // 쿠키 이름
		localeResolver.setCookieMaxAge(10000); // 쿠키 살려둘 시간, -1로 하면 브라우져를 닫을 때 없어짐.
		localeResolver.setCookiePath("/"); // Path를 지정해 주면 해당하는 path와 그 하위 path에서만 참조
		return localeResolver;
	}

	@Bean
	public MultipartResolver multipartResolver() throws IOException {
		// https://www.mkyong.com/spring-mvc/spring-mvc-file-upload-example/
		// Spring uses MultipartResolver interface to handle the file uploads web
		// application, two of the implementation
		// StandardServletMultipartResolver – Servlet 3.0 multipart request parsing.
		// CommonsMultipartResolver – Classic commons-fileupload.jar

		return new StandardServletMultipartResolver();

		// CommonsMultipartResolver resolver=new CommonsMultipartResolver();
		// resolver.setMaxUploadSize(1000000000); // 바이트 단위로 최대 용량을 지정한다.
		// resolver.setDefaultEncoding("UTF-8");
		// FileSystemResource resource = new FileSystemResource("./temp/");
		// resolver.setUploadTempDir(resource);
		// return resolver;

	}// :


	
	/**
	 * StringHttpMessageConverter를 생성합니다.
	 *
	 * @return the string http message converter
	 */
	@Bean
	StringHttpMessageConverter httpMessageConverter() {
		StringHttpMessageConverter converter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
		List<MediaType> mediaTypes = new ArrayList<>();
		mediaTypes.add(MediaType.TEXT_PLAIN);
		mediaTypes.add(MediaType.TEXT_HTML);
		converter.setSupportedMediaTypes(mediaTypes);
		return converter;
	}

	
	@Bean
	public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
		ByteArrayHttpMessageConverter arrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
		arrayHttpMessageConverter.setSupportedMediaTypes(getSupportedMediaTypes());
		return arrayHttpMessageConverter;
	}
	
	/**
	 * JacksonJsonConverter를 생성합니다.
	 * 
	 * @return
	 */
	@Bean
	// MappingJacksonHttpMessageConverter converter() {
	MappingJackson2HttpMessageConverter jacksonJsonConverter() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		// org.codehaus.jackson.map.ObjectMapper mpr2 = new
		// org.codehaus.jackson.map.ObjectMapper();
		converter.setObjectMapper(objectMapper());
		List<MediaType> mediaTypes = new ArrayList<>();
		mediaTypes.add(MediaType.APPLICATION_JSON);
		mediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
		mediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
		converter.setSupportedMediaTypes(mediaTypes);
		return converter;
	}
	/**
	 * Jackson ObjectMapper를 생성합니다.
	 * 
	 * @return
	 */
	@Bean
	public ObjectMapper objectMapper() {
		// ObjectMapper mapper = new ObjectMapper();
		// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		// mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);

		List<String> dateFormats = new ArrayList<String>();
		try {
			List<String> list = AppConfigUtil.readJSONToList("dateformat.json", String.class);
			dateFormats.addAll(list);
			LOG.info(" DateFormat 정의 파일을 읽었습니다.");

		} catch (Exception e) {
			// Ignores e
		}
		NaonObjectMapper mapper = new NaonObjectMapper(dateFormats);
		// SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		// mapper.setDateFormat(df);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setSerializerProvider(new CustomSerializerProvider());
		// serializer = object to json
		// deserializer = json to object
		return mapper;
	}//:
	
	/**
	 * MessageConverter를 등록합니다.
	 */
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
		messageConverters.add(httpMessageConverter());
		messageConverters.add(jacksonJsonConverter());
		messageConverters.add(byteArrayHttpMessageConverter());
		super.configureMessageConverters(messageConverters);
	}
	

	private List<MediaType> getSupportedMediaTypes() {
		List<MediaType> list = new ArrayList<MediaType>();
		list.add(MediaType.IMAGE_JPEG);
		list.add(MediaType.IMAGE_PNG);
		list.add(MediaType.APPLICATION_OCTET_STREAM);
		return list;
	}
	
}