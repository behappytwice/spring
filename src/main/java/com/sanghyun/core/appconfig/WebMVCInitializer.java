package com.sanghyun.core.appconfig;

import java.util.EnumSet;
import java.util.regex.Pattern;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import com.sanghyun.core.sitemesh.SitemeshFilter;

import ch.qos.logback.classic.util.ContextInitializer;

public class WebMVCInitializer implements WebApplicationInitializer {

	/** default servlewt name */
	private static final String DISPATCHER_SERVLET_NAME = "dispatcher";
	
	/** registers a character encoding filter */
	private void registerCharacterEncodingFilter(ServletContext servletContext) {
        FilterRegistration.Dynamic characterEncodingFilter = servletContext.addFilter("characterEncodingFilter", new CharacterEncodingFilter());
        characterEncodingFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        characterEncodingFilter.setInitParameter("encoding", "UTF-8");
        characterEncodingFilter.setInitParameter("forceEncoding", "true");
    }

	public void onStartup(ServletContext servletContext) throws ServletException {

		System.out.println("============================================================= WebMVCInitializer started.");
		
		setProfile();
		
		
//		// Create the root appcontext
//		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
//		rootContext.register(RootConfig.class);
//		// since we registered RootConfig instead of passing it to the constructor
//		rootContext.refresh();
//
//		// Create the web application context 
//		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
//		ctx.register(WebAppConfig.class);
//		servletContext.addListener(new ContextLoaderListener(ctx));
//		ctx.setServletContext(servletContext);
//
//		
//		
//		DispatcherServlet dispatcher = new DispatcherServlet(ctx);
//		//dispatcher.setThrowExceptionIfNoHandlerFound(true);;
////		Dynamic servlet = servletContext.addServlet(DISPATCHER_SERVLET_NAME, new DispatcherServlet(ctx));
//		Dynamic servlet = servletContext.addServlet(DISPATCHER_SERVLET_NAME, dispatcher);
//		servlet.addMapping("/");
//		servlet.setLoadOnStartup(1);

		
		// Create the root appcontext
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(RootConfig.class);
		
		System.out.println("============================================================= RootContext instantiated.");
		
		ContextLoaderListener contextLoader = new ContextLoaderListener(rootContext);
		servletContext.addListener(contextLoader);
		rootContext.setServletContext(servletContext);

		
//		// Create the web application context 
		AnnotationConfigWebApplicationContext webContext = new AnnotationConfigWebApplicationContext();
		webContext.register(WebAppConfig.class);
		//servletContext.addListener(new ContextLoaderListener(webContext));
		webContext.setServletContext(servletContext);

		
		registerCharacterEncodingFilter(servletContext);
			
			
		DispatcherServlet dispatcher = new DispatcherServlet(webContext);
		//dispatcher.setThrowExceptionIfNoHandlerFound(true);;
//		Dynamic servlet = servletContext.addServlet(DISPATCHER_SERVLET_NAME, new DispatcherServlet(ctx));
		Dynamic servlet = servletContext.addServlet(DISPATCHER_SERVLET_NAME, dispatcher);
		
		
		String TMP_FOLDER = "E:/upload";
		int MAX_UPLOAD_SIZE = 1024 * 1024 * 1024;
		MultipartConfigElement multipartConfigElement = new MultipartConfigElement(TMP_FOLDER, 
		          MAX_UPLOAD_SIZE, MAX_UPLOAD_SIZE * 2, MAX_UPLOAD_SIZE / 2);
		servlet.setMultipartConfig(multipartConfigElement);
	
		String[] mappings = { "/" };
		servlet.addMapping(mappings);
		servlet.setLoadOnStartup(1);

		addSitemeshFilterToServletContext(servletContext);

	}// :

	private void addSitemeshFilterToServletContext(ServletContext servletContext) {
		// https://github.com/HomoEfficio/dev-tips/blob/master/SpringMVC-JSP%20%ED%94%84%EB%A1%9C%EC%A0%9D%ED%8A%B8%EB%A5%BC%20SpringBoot%EB%A1%9C%20%EC%98%AE%EA%B8%B0%EA%B8%B0.md
		// https://codesilo.wordpress.com/2013/07/11/spring-mvc-with-sitemesh-3/
		FilterRegistration.Dynamic sitemesh = servletContext.addFilter("sitemesh", new SitemeshFilter());
		EnumSet sitemeshDispatcherTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD);
		sitemesh.addMappingForUrlPatterns(sitemeshDispatcherTypes, true, "/*");
	}// :
	
	
	private void setProfile() {
		if(System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME) == null) { 
			// 지정된 profile이 없는 경우 production 환경을 프로파일로 설정
			System.setProperty(AppConfigConstants.SANGHYUN_PROFILE, AppConfigConstants.PROFILE_DEVELOPMENT);
			System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, AppConfigConstants.PROFILE_DEVELOPMENT);
			
			// 프로파일 경로에 있는 logback.xml 설정
			//System.setProperty( ENV_LOGBACK_CONFIG_FILE, ENV_PRODUCTION);
			ClassPathResource resource = new ClassPathResource("profile/" + AppConfigConstants.PROFILE_DEVELOPMENT + "/logback.xml");
			try { 
				//LogbackConfigurer.initLogging(resource.getFile().getAbsolutePath());
				// profile 경로에 있는 logback.xml을 읽어 들인다. 
				System.setProperty(ContextInitializer.CONFIG_FILE_PROPERTY, resource.getFile().getAbsolutePath());
				System.out.println("#################################################################################");
				System.out.println("#                                                                               #");
				System.out.println("#           개발서버 환경에서 그룹웨어를 시작합니다.                            #");
				System.out.println("#                                                                               #");				
				System.out.println("#################################################################################");
				
			}catch(Exception e) { 
				System.out.println("logback.xml이 지정된 경로에 없습니다.");
			}
		} else {
			
			String profileProperty = System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME);
			
//			if(profileProperty.indexOf(AppConfigConstants.PROFILE_SQLLOG) < 0) { 
//				System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME,  profileProperty + "," + AppConfigConstants.PROFILE_NOSQLLOG);
//			}
			
			String[] profileNames = profileProperty.split(Pattern.quote(","));
			for(String profileName : profileNames)  { 
				if(profileName.trim().equals(AppConfigConstants.PROFILE_SQLLOG)) {
					continue; 
				}else {
							
					if(profileName.trim().equals(AppConfigConstants.PROFILE_PRODUCTION)) {
						System.out.println("#################################################################################");
						System.out.println("#                                                                               #");
						System.out.println("#           운영서버 환경에서 그룹웨어를 시작합니다.                            #");	
						System.out.println("#                                                                               #");
						System.out.println("#################################################################################");
						
					}else if(profileName.trim().equals(AppConfigConstants.PROFILE_STAGING)) {
						System.out.println("#################################################################################");
						System.out.println("#                                                                               #");
						System.out.println("#           검증서버 환경에서 그룹웨어를 시작합니다.                            #");	
						System.out.println("#                                                                               #");
						System.out.println("#################################################################################");						
					}else if(profileName.trim().equals(AppConfigConstants.PROFILE_DEVELOPMENT)) {			
						System.out.println("#################################################################################");
						System.out.println("#                                                                               #");
						System.out.println("#           개발서버 환경에서 그룹웨어를 시작합니다.                            #");	
						System.out.println("#                                                                               #");
						System.out.println("#################################################################################");						

					}else { 
						System.out.println(profileName.trim() + "서버 환경에서 그룹웨어를 시작합니다.");
						System.out.println("#################################################################################");
						System.out.println("#                                                                               #");
						System.out.println("#           " + profileName.trim() + "서버 환경에서 그룹웨어를 시작합니다.                            #");	
						System.out.println("#                                                                               #");
						System.out.println("#################################################################################");						

					}
					
					System.setProperty(AppConfigConstants.SANGHYUN_PROFILE, profileName.trim());
					// profile이 있는 경우 profile/ 아래의 profile 이름의 경로에 있는 logback.xml을 읽는다. 
					ClassPathResource resource = new ClassPathResource("profile/" + profileName.trim() + "/logback.xml");
					try { 
						System.setProperty(ContextInitializer.CONFIG_FILE_PROPERTY, resource.getFile().getAbsolutePath());
					}catch(Exception e) { 
						System.out.println("logback.xml이 지정된 경로에 없습니다.");
					}
					break;
				}
			}
		}
		
	}//:

}