package com.sanghyun.core.appconfig;

public class AppConfigConstants {

	public static final String CONST_BASE_DODMAIN_PATH = "com/sanghyun";
	
	/** 운영환경 프로파일 명 */
	public static final String PROFILE_PRODUCTION = "production";
	/** 개발환경 프로파일 명 */ 
	public static final String PROFILE_DEVELOPMENT = "development";
	/** 검증환경 프로파일 명 */ 
	public static final String PROFILE_STAGING = "staging";
	/** 실행 SQL 로깅을 위한 프로파일 명 */ 
	public static final String PROFILE_SQLLOG = "sqllog";
	/** 실행 SQL 로깅을 하지 않기 위한 프로파일 명 */
	public static final String PROFILE_NOSQLLOG = "nosqllog";
	/** 어플리케이션 실행환경 */
	public static final String SANGHYUN_PROFILE = "sanghyun.profile";
	
	/** @deprecated */
	public static final String PROPERTY_PROFILE_DEV = "dev";
	/** @deprecated */
	public static final String PROPERTY_PROFILE_STG = "stg";
	/** @deprecated */
	public static final String PROPERTY_PROFILE_PROD = "prod";

	public static final String PROPERTY_NAME_DEV_DATABASE_DRIVER = "dev.db.driver";
	public static final String PROPERTY_NAME_DEV_DATABASE_PASSWORD = "dev.db.password";
	public static final String PROPERTY_NAME_DEV_DATABASE_URL = "dev.db.url";
	public static final String PROPERTY_NAME_DEV_DATABASE_USERNAME = "dev.db.username";

	public static final String PROPERTY_NAME_STG_DATABASE_DRIVER = "stg.db.driver";
	public static final String PROPERTY_NAME_STG_DATABASE_PASSWORD = "stg.db.password";
	public static final String PROPERTY_NAME_STG_DATABASE_URL = "stg.db.url";
	public static final String PROPERTY_NAME_STG_DATABASE_USERNAME = "stg.db.username";

	public static final String PROPERTY_NAME_PROD_DATABASE_DRIVER = "prod.db.driver";
	public static final String PROPERTY_NAME_PROD_DATABASE_PASSWORD = "prod.db.password";
	public static final String PROPERTY_NAME_PROD_DATABASE_URL = "prod.db.url";
	public static final String PROPERTY_NAME_PROD_DATABASE_USERNAME = "prod.db.username";

	public static final String PROPERY_NAME_JNDI_NAME = "jndi.name";
	public static final String PROPERY_NAME_DB_TYPE = "db.type";

	public static final String PROPERTY_NAME_HIBERNATE_DIALECT = "hibernate.dialect";
	public static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "hibernate.show_sql";
	public static final String PROPERTY_NAME_HIBERNATE_HDM2DDL_AUTO = "hibernate.hbm2ddl.auto";
	public static final String PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN = "entitymanager.packages.to.scan";

	
	public static final String PROPERTY_NAME_ACTIVEMQ_BROKER_URL = "activemq.broker.url";
	public static final String PROPERTY_NAME_ACTIVEMQ_QUEUE_NAME = "order-queue";
	public static final String PROPERTY_NAME_ACTIVEMQ_QUEUE_RESPONSE_NAME = "order-response-queue";

}/// ~
