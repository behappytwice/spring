package com.sanghyun.core.appconfig;

import java.util.Properties;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.sanghyun.core.log.Log;

@Configuration
public class JPAAppConfig {

	@Resource
	protected Environment env;
	protected static @Log Logger LOG;
	
	@Autowired
	//@Qualifier("dataSource")
	DataSource dataSource; 

	// http://www.baeldung.com/spring-persistence-jpa-jndi-datasource

	private Properties hibProperties() {
		Properties properties = new Properties();
		properties.put(AppConfigConstants.PROPERTY_NAME_HIBERNATE_DIALECT,
				env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_HIBERNATE_DIALECT));
		properties.put(AppConfigConstants.PROPERTY_NAME_HIBERNATE_SHOW_SQL,
				env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_HIBERNATE_SHOW_SQL));
		properties.put(AppConfigConstants.PROPERTY_NAME_HIBERNATE_HDM2DDL_AUTO,
				env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_HIBERNATE_HDM2DDL_AUTO));
		return properties;
	}
	
	
	/// http://www.baeldung.com/spring-persistence-jpa-jndi-datasource
	//  http://www.baeldung.com/spring-persistence-jpa-jndi-datasource 

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NamingException {

		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		//entityManagerFactoryBean.setDataSource(jpaDataSource());
		entityManagerFactoryBean.setDataSource(dataSource);
		// entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistence.class);
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		entityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
		entityManagerFactoryBean.setPackagesToScan(
				env.getRequiredProperty(AppConfigConstants.PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));
		entityManagerFactoryBean.setJpaProperties(hibProperties());
		return entityManagerFactoryBean;
	}
	
	
	@Bean
	@Qualifier("jpaTransactionManager")
	public JpaTransactionManager jpaTransactionManager() throws NamingException  {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}

}/// ~
