package com.sanghyun.core.appconfig;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RestController;

import com.sanghyun.core.log.Log;
import com.sanghyun.core.util.AppConfigUtil;


@Configuration
@ComponentScan(basePackages = { "com.sanghyun" } // com.sanghyun package 아래를 모두 스캔한다. 
//, includeFilters = {
////@ComponentScan.Filter(type = FilterType.REGEX,
//pattern="com.test.External*")
//@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
//value=com.test.ExternalComponentScan.class)
//}
		, excludeFilters = {
				 @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Controller.class),
				 @Filter(RestController.class),
//				 @ComponentScan.Filter(type = FilterType.ANNOTATION, value =
				// Configuration.class)
				// @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
				// value=WebAppConfig.class)
				@ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.sanghyun.*.*JNDIDataSourceAppConfig+"),				 
				@ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.sanghyun.*.*JUNITAppConfig+"),
				@ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.sanghyun.*.*WebAppConfig+")
})
@PropertySources({ @PropertySource("classpath:application.properties"),
	@PropertySource(value = "classpath:application-site.properties", ignoreResourceNotFound = true), 
	@PropertySource(value = "classpath:application-extension.properties", ignoreResourceNotFound = true) 
})
@EnableTransactionManagement 
public class RootConfig {
	
	
	@Resource
	private Environment env;
	protected static @Log Logger LOG;	

	@Bean
	public ResourceBundleMessageSource messageSource() {
		
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();		
		try {
			
			List<String> list  = AppConfigUtil.readJSON("i18n.json");
			List<String> list2 = AppConfigUtil.readJSON("i18n-extension.json");
			list.addAll(list2);
			
			String[] messageList = new String[list.size()];
			messageList = list.toArray(messageList);
			source.setBasenames(messageList);
			//source.setBasename(env.getRequiredProperty("message.source.basename"));
			source.setUseCodeAsDefaultMessage(true);
		}catch(Exception e) { 
			// Ignores exceptions
		}
		return source; 
	}
	
	@Bean
	public ThreadPoolTaskExecutor threadPoolTaskExecution() {
		ThreadPoolTaskExecutor tp = new ThreadPoolTaskExecutor();
		tp.setCorePoolSize(25);
		tp.setMaxPoolSize(100);
		return tp;
	}
	
	   @Bean(name = "applicationEventMulticaster")
	    public ApplicationEventMulticaster simpleApplicationEventMulticaster() {
	        SimpleApplicationEventMulticaster eventMulticaster 
	          = new SimpleApplicationEventMulticaster();
	         
	        //eventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
	        eventMulticaster.setTaskExecutor(threadPoolTaskExecution());
	        return eventMulticaster;
	    }
	
}///~
