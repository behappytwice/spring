package com.sanghyun.core.beans;

import java.util.List;

/**
 * 목목에 객체를 담기 위한 Generic class입니다. 
 * 
 * @author Sanghyun,Kim(sanghyun@naonsoft.com)
 *
 * @param <T>
 */
public class ObjectArrayForJSON<T> {
	/**
	 * 객체 목록 
	 */
	private List<T> items;

	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

}
