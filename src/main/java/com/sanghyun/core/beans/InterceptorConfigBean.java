package com.sanghyun.core.beans;

/**
 * Interceptor 등록을 위한 설정파일을 담을 클래스입니다.
 * @author Sanghyun,Kim(sanghyun@naonsoft.com)
 *
 */
/**
 * @author Sanghyun,Kim(sanghyun@naonsoft.com)
 *
 */
public class InterceptorConfigBean {

	/** 
	 * 인터셉터의 이름
	 */
	private String name;
	/**
	 * 인터셉터의 설명 
	 */
	private String description;
	/**
	 *  인터셉터가 처리할 URL Path
	 * 
	 */
	private String[] addPathPattern;
	/**
	 * Interceptor가 처리하지 않을 URL Path
	 */
	private String[] excludePathPattern;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String[] getAddPathPattern() {
		return addPathPattern;
	}
	public void setAddPathPattern(String[] addPathPattern) {
		this.addPathPattern = addPathPattern;
	}
	public String[] getExcludePathPattern() {
		return excludePathPattern;
	}
	public void setExcludePathPattern(String[] excludePathPattern) {
		this.excludePathPattern = excludePathPattern;
	}
}
