package com.sanghyun.sample.upload;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sanghyun.core.base.BaseController;

@Controller
@RequestMapping(value = "/{sitemesh}/sample/upload")
public class SampleUploadController extends BaseController {
	
	@RequestMapping(value = "/uploadMain")
	public String uploadMain(HttpServletRequest request, HttpServletResponse response) {
		return "sample/upload/ftlUploadMain"; 
	}//:
	
	@RequestMapping(value = "/uploadFile", produces="application/json")
	public @ResponseBody SampleFileInfo  upload(@RequestParam("file") MultipartFile file) throws Exception {
		SampleFileInfo sf = new SampleFileInfo();
		System.out.println(file.getName() + "//" + file.getOriginalFilename());
		Path path = Paths.get("E:/upload/" + file.getOriginalFilename());
		Files.write(path, file.getBytes());
		sf.setFileName(file.getOriginalFilename());
		return sf; 
	}//:
	
}///~
