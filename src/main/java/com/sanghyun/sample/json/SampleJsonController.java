package com.sanghyun.sample.json;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sanghyun.core.base.BaseController;

@Controller
@RequestMapping(value = "/{sitemesh}/sample/json")
public class SampleJsonController extends BaseController  {

	
	@RequestMapping(value="/main")
	public ModelAndView createView() {
		ModelAndView mv = new ModelAndView("sample/json/ftlJson");
		return mv; 
	}//:	
	
	
	@RequestMapping(value = "/internalError")
	public void internalError(Model model) {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> internalError ");
		model.addAttribute("errorMessage", "error");
		throw new RuntimeException("internal Error");
	}//:
	
	
	@RequestMapping(value = "/getJson", produces="application/json")
	public @ResponseBody SampleJsonAddressBean getJson(Model model) {
		SampleJsonAddressBean bean = new SampleJsonAddressBean();
		bean.setCity("Seoul");
		bean.setStreet("Seochoro 21");
		bean.setZipCode(121);
		
		return bean;
		
	}//:
		
	
}///~
