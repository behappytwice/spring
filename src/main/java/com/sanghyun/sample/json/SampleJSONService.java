package com.sanghyun.sample.json;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sanghyun.core.base.BaseService;
import com.sanghyun.core.util.ClassPathFileUtil;


@Service
public class SampleJSONService extends BaseService {
	
	
	public SampleJsonEmployeeBean readJsonWithObjectMapper() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		//SampleJsonEmployeeBean emp = objectMapper.readValue(new File("employee.json"), SampleJsonEmployeeBean.class);
		SampleJsonEmployeeBean emp = objectMapper.readValue(ClassPathFileUtil.getFileObject("com/sanghyun/sample/json/employee.json"), SampleJsonEmployeeBean.class);
		return emp;
	}//:
	

}/// ~
