package com.sanghyun.sample.vuejs;

public class Message {
	
	/** this message's ID **/
	private String messageId;
	/** the response code */
	private String responseCode;
	/** the message to be returned by application logic */
	private String responseMessage;
	/** if application's error occurs, this contains internal system error message */
	private String internalErrorMessage;
	/** the data to be returned from server */
	private Object data;
	
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getInternalErrorMessage() {
		return internalErrorMessage;
	}
	public void setInternalErrorMessage(String internalErrorMessage) {
		this.internalErrorMessage = internalErrorMessage;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	} 
	

}
