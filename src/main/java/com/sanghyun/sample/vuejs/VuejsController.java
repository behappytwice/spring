package com.sanghyun.sample.vuejs;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/{sitemesh}/vuejs")
public class VuejsController {

	
	
	
	@RequestMapping(value = "/getJsonEmployee", produces="application/json")
	public @ResponseBody Message service() {
		JsonEmployee emp = new JsonEmployee();
		emp.setName("KIM");
		emp.setUserId("AI2098773");
		Message message = new Message();
		message.setData(emp);
		return message;
		
	}//:
	
	
	
	@RequestMapping(value = "/getJsonEmployee2", produces="application/json")
	public @ResponseBody JsonEmployee getJson() {
		JsonEmployee emp = new JsonEmployee();
		emp.setName("KIM");
		emp.setUserId("AI2098773");
		return emp;
		
	}//:
	
}
