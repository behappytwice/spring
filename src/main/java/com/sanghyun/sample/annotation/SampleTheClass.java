package com.sanghyun.sample.annotation;

import java.lang.annotation.Annotation;

@SampleMyAnnotation(name="someName", value="Hello World")
public class SampleTheClass {
	public SampleTheClass() {
	}
	public void display() {
		Class aClass = SampleTheClass.class;
		Annotation[] annotations = aClass.getAnnotations();
		for(Annotation annotation : annotations){
		    if(annotation instanceof SampleMyAnnotation){
		        SampleMyAnnotation myAnnotation = (SampleMyAnnotation) annotation;
		        System.out.println("name: " + myAnnotation.name());
		        System.out.println("value: " + myAnnotation.value());
		    }
		}
	}
}
