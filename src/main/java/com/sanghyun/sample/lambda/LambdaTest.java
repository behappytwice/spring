package com.sanghyun.sample.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.junit.Test;

/**
 * 
 * Refert to <br> 
 * 1) https://jdm.kr/blog/181 <br>
 * 2) https://blog.hanumoka.net/2018/11/16/java-20181116-java-lambda/ <br>
 * 3) https://imcts.github.io/java-method-reference/ <br>
 * 4) https://www.baeldung.com/java-8-functional-interfaces
 * 
 * @author Andy
 *
 */
public class LambdaTest {

	@Test
	public void testLambda() {
		new Thread(()-> {
			System.out.println("Hello World");
		}).start();
	}//:

	
	@Test 
	public void testFunctionalInterface() {
		Func add = ( int a, int b) -> { return a + b;};
		System.out.println(add.calc(1,2));
		
	}//:

	@Test
	public void testMethodReference() {
		
		String[] strings = new String[] { "6", "5", "4", "3", "2", "1" };
		List<String> list = Arrays.asList(strings);
		//list.forEach(x -> System.out.println(x));
		// 위의 식을 간단하게 아래처럼 표현이 가능 
		list.forEach(System.out::println);
		
		
	}// :
	
	/**
	 * Refert to : https://www.baeldung.com/java-8-functional-interfaces
	 */
	@Test
	public void testFunction() {
		
		// 인자와 리턴값으로 표현 
		// public interface Function<T, R> { … }
		
		Function<Integer, String> intToString = Object::toString;
		System.out.println(intToString.apply(100));
		
	}// :

}
