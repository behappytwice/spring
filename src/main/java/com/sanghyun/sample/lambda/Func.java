package com.sanghyun.sample.lambda;

@FunctionalInterface
public interface Func {

	public int calc(int a, int b);
}
