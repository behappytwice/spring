package com.sanghyun.sample.crypt;

import java.security.PrivateKey;
import java.security.PublicKey;

public class SampleCryptBean {

	/** 공개키의 모듈을 hex로 변경한 값 */
	private String publicKeyModule;
	/** 공개키의 exponent를 hex로 변경한 값 */
	private String publicKeyExponent;
	/** RSA 공개키 */ 
	private PublicKey publicKey;
	/** RSA 비밀키 */ 
	private PrivateKey privateKey; 
	
	public PublicKey getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}
	public PrivateKey getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}
	public String getPublicKeyModule() {
		return publicKeyModule;
	}
	public void setPublicKeyModule(String publicKeyModule) {
		this.publicKeyModule = publicKeyModule;
	}
	public String getPublicKeyExponent() {
		return publicKeyExponent;
	}
	public void setPublicKeyExponent(String publicKeyExponent) {
		this.publicKeyExponent = publicKeyExponent;
	} 
	
	
}///~
