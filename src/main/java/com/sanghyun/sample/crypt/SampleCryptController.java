package com.sanghyun.sample.crypt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sanghyun.core.base.BaseController;

@Controller
@RequestMapping(value = "/{sitemesh}/sample/crypt")
public class SampleCryptController extends BaseController {

	private static final String CONST_SESSION_NAME="SAMPLE";
	
	@Autowired
	private SampleCryptService sampleCrypteService;

	
	@RequestMapping(value="/ftlCryptMain", method=RequestMethod.GET)  
	public String ftlCryptMain(HttpServletRequest request) {
		SampleCryptBean bean = sampleCrypteService.getPublicKeyModuleAndExponent();
		
		HttpSession session = request.getSession(true);
		session.setAttribute(CONST_SESSION_NAME, bean);
		
//		System.out.println(bean.getPublicKeyModule());
//		System.out.println(bean.getPublicKeyExponent());

		request.setAttribute("cryptBean", bean); 
		return "sample/crypt/ftlCryptMain";
		
	}// :
	
	@RequestMapping(value="/login", method=RequestMethod.POST)  
	public @ResponseBody SampleCryptLoginBean login(HttpServletRequest request, HttpServletResponse response,  @RequestBody SampleCryptLoginBean loginBean)  throws Exception {
		HttpSession session = request.getSession();
		if(session == null) {
			LOG.error("Sessoin not found");
			throw new RuntimeException("Session not found");
		}
		
		LOG.info("USER ID:" + loginBean.getUserId());
		LOG.info("PASSWORD:" + loginBean.getPassword());
		
		
		SampleCryptBean bean = (SampleCryptBean)session.getAttribute(CONST_SESSION_NAME);
		if(bean == null) { 
			LOG.error(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SessionBean is null.");
		}
		
		
		String decryptedUserId   = this.sampleCrypteService.decrypt(loginBean.getUserId(), bean);
		String decryptedPassword = this.sampleCrypteService.decrypt(loginBean.getPassword(), bean);
		SampleCryptLoginBean returnBean = new SampleCryptLoginBean();
		returnBean.setUserId(decryptedUserId);
		returnBean.setPassword(decryptedPassword);
		return returnBean; 
	}// :
	
	
	
}/// ~
