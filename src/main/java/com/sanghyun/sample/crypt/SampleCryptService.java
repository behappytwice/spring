package com.sanghyun.sample.crypt;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;

import org.springframework.stereotype.Service;

import com.sanghyun.core.base.BaseService;
import com.sanghyun.core.crypt.RSAHelper;
import com.sanghyun.core.crypt.RSAUtil;

@Service
public class SampleCryptService extends BaseService {

	/**
	 * 공개키의 module과 exponent값을 16진수로 변환한 값을 반환한다.
	 * 
	 * @return
	 */
	public SampleCryptBean getPublicKeyModuleAndExponent() {
		SampleCryptBean bean = new SampleCryptBean();
		try {

			// KeyPair keyPair = RSAHelper.createKeyPair();
			// PublicKey publicKey = keyPair.getPublic();
			// PrivateKey privateKey = keyPair.getPrivate();
			// RSAPublicKeySpec publicSpec = RSAHelper.getRSAPublicKeySpec(publicKey);

			KeyPair keyPair = RSAUtil.generateKeyPair();
			PublicKey publicKey = keyPair.getPublic();
			PrivateKey privateKey = keyPair.getPrivate();
			RSAPublicKeySpec publicSpec = RSAUtil.getRSAPublicKeySpec(publicKey);

            bean.setPrivateKey(privateKey);
			bean.setPublicKey(publicKey);
			bean.setPublicKeyModule(publicSpec.getModulus().toString(16));
			bean.setPublicKeyExponent(publicSpec.getPublicExponent().toString(16));

		} catch (Exception e) {
			// Ignores exception
			LOG.error("암호화키를 생성 중에 오류가 발생하였습니다.");
			throw new RuntimeException();
		}
		return bean;
	}// :

	public String decrypt(String encryptedString, SampleCryptBean bean) throws Exception {

		LOG.info(">> encrypted String >" + encryptedString);
		return RSAUtil.decrypt(encryptedString, bean.getPrivateKey());
	}// :

}/// ~
