package com.sanghyun.sample.mybatis;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;

//@Entity
//@Table(name="ORG_EMPLOYEE")
public class SampleMybatisEmployeeBean {
	
	//@Id
	//@Column(name="EMP_ID", updatable=false)
	private String empId;
	
	//@Column(name="USER_ID")
	private String userId;
	
	//@Column(name="EMP_NO")
	private String empNo;
	
	//@Column(name="USER_NAME")
	private String userName;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}///~
