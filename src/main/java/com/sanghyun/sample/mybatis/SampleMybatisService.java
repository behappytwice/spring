package com.sanghyun.sample.mybatis;

import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sanghyun.core.base.BaseService;

@Service
public class SampleMybatisService  extends BaseService {
	@Resource
	private SampleMybatisDAO sampleDAO; 

	public List<SampleMybatisEmployeeBean> selectAllEmployee() {
		return sampleDAO.selectAllEmployee();
	}
	
}///~