package com.sanghyun.sample.mybatis;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sanghyun.core.base.BaseDAO;


@Repository
public class SampleMybatisDAO extends BaseDAO {

	/** The namespace. */
	private String namespace = "com.sanghyun.sample.mybatis.SampleMybatisEmployeeDAO";
	
	public List<SampleMybatisEmployeeBean> selectAllEmployee() {
		return this.sqlSession.selectList(namespace + ".selectAll"); 
	}
	
}
