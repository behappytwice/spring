package com.sanghyun.sample.mybatis;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sanghyun.core.base.BaseController;

@Controller
@RequestMapping(value="/sample/mybatis")
public class SampleMybatisController extends BaseController {

	@Autowired
	private SampleMybatisService sampleService;
	
	@RequestMapping(value="/main")
	public ModelAndView createView() {
		ModelAndView mv = new ModelAndView("biz/sample/sampleMain");
		return mv; 
	}//:
	
	@RequestMapping(value="/echoEmployee", method=RequestMethod.POST)  
	public @ResponseBody SampleMybatisEmployeeBean echoEmployee(HttpServletRequest request, HttpServletResponse response,  @RequestBody SampleMybatisEmployeeBean employee)  {
		LOG.info(employee.getEmpId());
		LOG.info(employee.getEmpNo());
		LOG.info(employee.getUserId());
		LOG.info(employee.getUserName());
		return employee;
	}//:

	@RequestMapping(value="/employeeList", method=RequestMethod.POST)  
	public @ResponseBody List<SampleMybatisEmployeeBean>  selectEmployeeList(HttpServletRequest request, HttpServletResponse response,  @RequestBody SampleMybatisEmployeeBean employee)  {
		return this.sampleService.selectAllEmployee();
	}//:-

	
	@RequestMapping(value="/ftlEmployee", method=RequestMethod.GET)  
	public String ftlEmployee() {
		return "sample/ftlEmployee";
	}//:
	
}///~
