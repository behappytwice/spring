package com.sanghyun.sample.nio;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.junit.Test;

/**
 * 
 * Refer to : <br>
 * 1) http://tutorials.jenkov.com/java-nio/path.html <br>
 * 2) http://tutorials.jenkov.com/java-nio/files.html  <br>
 * 3) https://docs.oracle.com/javase/tutorial/essential/io/pathOps.html <br>
 * @author Andy
 *
 */
public class NioTest {

	@Test
	public void testCreatingPath() {
		
		// creating an absolute path
//		Path path = Paths.get("C:\\temp\\textfile1.txt");
//		
//		// creating a relative path 
//		Path projects = Paths.get("d:\\data", "projects");   // d:/data/projects
//		Path file     = Paths.get("d:\\data", "projects\\a-project\\myfile.txt"); // d:/data/a-project/myfile.txt 
		
		// . means current directory. .. means parent directory  
		Path currentDir = Paths.get(".");
		System.out.println(currentDir.toAbsolutePath());
		
	}//:
	
	@Test
	public void testNormalize() {
		
		// Path.normalize()
		// normalize는 . 과 .. 을 제거한다. 
		
		String originalPath =
		        "d:\\data\\projects\\a-project\\..\\another-project";

		Path path1 = Paths.get(originalPath);
		System.out.println("path1 = " + path1);

		Path path2 = path1.normalize();
		System.out.println("path2 = " + path2);

	}//;
	
	@Test
	public void testJoiningTwoPaths() {
		// Microsoft Windows
		Path p1 = Paths.get("C:\\home\\joe\\foo");
		// Result is C:\home\joe\foo\bar
		System.out.format("%s%n", p1.resolve("bar"));
		
	}
	
	@Test
	public void testFilesWalk() throws IOException {
		Path start = Paths.get("E:\\temp");
		Stream<Path> stream = Files.walk(start, 3, FileVisitOption.FOLLOW_LINKS);
		stream.limit(100).forEach(System.out::println);
	
		// 상대경로로 바꾼다(relativize)tj
		stream = Files.walk(start, 3, FileVisitOption.FOLLOW_LINKS).map(start::relativize);
		stream.limit(10).forEach(System.out::println);
		
		
	}
	
}
