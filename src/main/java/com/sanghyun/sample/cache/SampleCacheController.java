package com.sanghyun.sample.cache;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/{sitemesh}/sample/cache")
public class SampleCacheController {

	@RequestMapping(value="/main")
	public ModelAndView createView() {
		ModelAndView mv = new ModelAndView("sample/sitemesh/ftlSitemesh");
		return mv; 
	}//:
}
