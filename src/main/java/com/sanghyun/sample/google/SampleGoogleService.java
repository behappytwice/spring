package com.sanghyun.sample.google;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.sanghyun.core.base.BaseService;


@Service
/**
 * This sevice class is meant to explain the usage of Google API
 * 
 * @author Andy
 *
 */
public class SampleGoogleService extends BaseService {

	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param token
	 * @throws Exception
	 */
	public void syncCalendar(HttpServletRequest request, HttpServletResponse response,TokenResponse token) throws Exception {
		
		String APPLICATION_NAME = "Naonsoft Groupware";
		JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
		GoogleCredential credential = new GoogleCredential().setAccessToken(token.getAccessToken());
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
        
        // List the next 10 events from the primary calendar.
        DateTime now = new DateTime(System.currentTimeMillis());
        Events events = service.events().list("primary")
                .setMaxResults(20)
                .setTimeMin(now)
                .setOrderBy("startTime")
                .setSingleEvents(true)
                .execute();
        List<Event> items = events.getItems();
        if (items.isEmpty()) {
            System.out.println("No upcoming events found.");
        } else {
            System.out.println("Upcoming events");
            for (Event event : items) {
                DateTime start = event.getStart().getDateTime();
                if (start == null) {
                    start = event.getStart().getDate();
                }
                System.out.printf("%s (%s)\n", event.getSummary(), start);
            }
        }
        
		HttpRequestFactory requestFactory  = new NetHttpTransport().createRequestFactory();
		HttpRequest request22 = requestFactory.buildGetRequest(
				new GenericUrl("https://www.googleapis.com/calendar/v3/users/me/calendarList")
				  .set("access_token", token.getAccessToken())
				);
		String rawResponse = request22.execute().parseAsString();
		System.out.println(rawResponse);
		
	}//:
	
}///~
