# OAuth2 

OAuth 2.0 flow는 사용자 인증을 위한 것이고 기밀의 정보를 저장하고 유지할 수 있는 
application을 위해 설계된 것 
적절히 인증된 web server application은 사용자가
application과 상호작용하거나 applicaiton을 떠난 후에 API에 접근할 수 있다.

web server application은 자주 API 요청들에게 권한을 부여하기 위해
service account를 사용할 수 있다. 



## Step 1. Set authorization parameters

client_id      application에 대한 client ID 
redirect_uri   사용자가 인증을 완료한 이후에 API server가 redirect할 URL을 결정한다. 
scope 
  공백(space)으로 구분된(정해진) scopes의 리스트 
  scope는 https://www.googleapis.com/auth/calendar와 같은 URL 문자열이다.
  

access_type 
 사용자가 브라우저에 없을 때 access token을 갱신(refresh)할지를 나타낸다(indicated)
 유효한 값들은 online과 offline이고 default value는 online
 
 사용자가 브라우저에 없을 때 애플리케이션이 access token을 갱신할 필요가 있다면
 offline으로 값을 설정한다. 
 
 
 
 
## Step 2. Redirect to Google's OAuth 2.0 server 
application이 처음으로 사용자의 데이타에 접근할 필요가 있을 때 
인증 프로세스와 인증을 시작(initiate)하기 위해 구글 OAuth 2.0 server에 
사용자를 redirect한다. 

구글의 OAuth 2.0 서버는 사요자를 인증하고 요청된 범위(requested scopes)에 접근하기 위해
당신의 애플리케이션을 사용자로 부터 동의를 얻는다. 
redirect URL을  사용하여 당신의 애플리케이션으로 응답을 되돌린다. 


## Step 3. Google prompt user for consent 

구글이 동의를 구하는 창을 표시하고 사용자는 애플리케이션에 요청된
접근을 권한을 부여할지를 결정하낟. 



## Step 4. Handle the OAuth 2.0 server response 

OAuth 2.0 server는 당신의 앱의 접근 요청에 대해 응답하는데
요청에서 명시된 URL을 사용한다. 

사용자가 요청을 승인하면, 응답은 인증코드(authorization code)를 포함한다. 
사용자가 용청을 허락하지 안으면, 응답은 에러 메시지를를 포함한다.

웹서버에 반환되는 인증코드나 에러메시지는 query string에 나타난다. 다음과 같다. 


error response: 

https://oauth2.example.com/auth?error=access_denied


authorization code response: 

https://oauth2.example.com/auth?code=4/P7q7W91a-oMsCeLvIaQm6bTrgtp7



## Step 5. Exchange authorization code for refresh and access tokens
 
web server가 인증코드를 받은 후에
access token에 대한 인증코드를 교환할 수 있다. 


access token에 대한 인증코드를 교환하기 위해 

https://www.googleapis.com/oauth2/v4/token endpint를 호출하고 다음의 
파라미터들을 설정한다. 


code : 시작 요청세서 반환된 인증코드
client_id  : client id 
client_secret : client secret 
redirect_uri : redirect URI 
grant_type : 
  OAuth 2.0 명세세 정의된것처러 , 이 필드는 authorization_code 값을 포함해야 한다
  
  
POST /oauth2/v4/token HTTP/1.1
Host: www.googleapis.com
Content-Type: application/x-www-form-urlencoded

code=4/P7q7W91a-oMsCeLvIaQm6bTrgtp7&
client_id=your_client_id&
client_secret=your_client_secret&
redirect_uri=https://oauth2.example.com/code&
grant_type=authorization_cod


응답은 다음의 필드들을 포함한다. 

access_token 
   Google API 요청을 인증하기 위해 당신의 애플리케이션이 보낸 token 
   
refresh_token
    새로은 접근 토큰(access token)을 얻기 위해 사용할 token 
    Refrersh token은 사용자가 접근을 철회(revoke)까지 유효하다. 
    다시말하자면, 
    초기에 이 필드는 access_type 파라미터가 offline으로 설정된 경우에만 
    나타난다. 

expires_in 
    남은 시간(remaing lifetime) , 초단위   

token_type 
    반환되는 토큰 타입. 이 시점에서, 이 필드의 값은 항상 Bearer 이다. 


{
  "access_token":"1/fFAGRNJru1FTz70BzhT3Zg",
  "expires_in":3920,
  "token_type":"Bearer",
  "refresh_token":"1/xEoDL4iW3cxlI7yDbSRFYNG01kVKM2C-259HOF2aQbI"
}




# Handling Access Token Expiration
    
    
Refer to  http://usefulangle.com/post/11/php-google-apis-handling-access-token-expiration

    
access token은 만료 시간 같은 것이 있다. 
    


## Step 1. Request for Offline Access 

구글은 offline access를 제공하는데
이것은 온라인 상태가 아닌 사용자를 대신하여 API Calls를 할 수 있다는 것을 의미한다. 
애플리케이션은 로그인 시에 사용자로 부터 offline access 권한을 요청해야 한다. 
access_type=offline 파라미터를 사용한다. 


## Step 2 Saving the Refresh Token and Access token Expiration Timestamp 

API call의 응답은 다음을 포함할 것이다. 


access_token
refresh_token
expires_in


expires_in은 시간(초 단위)를 제공한다. 보통 3600 seconds이다. 
이것은 access token은 한시간 이후에 만료된다는 것을 의미한다. 


refresh token의 요첨은 access token을 refresh하는 것이다. 
당신은 refreshh token을 당신의 데이터베이스에 저장해야 한다. 
Google 은 사용자가 승인 화면(consent screen)
에서 애플리케이션에 권한을 승인하는 경우에만 refresh token을  전달하기 때문이다. 


그 이후에 
사용자가 다시 로그인을 할 때
그는 동의화면(consent screen)을 보지 못할 것이다. 
이것은 구글에 의해서 전달되는 refresh token이 없다는 것을 의미한다. 


새로운 refresh token을 얻기 위해서, 
당신은 사용자에게 다시 consent screen을 보여야할 필요가 있다. 

동의를 사용자에게 다시 유도하는 것은 login url에 prompt 파라미터를
추가하여 할 수 있다. 

prompt=consent

현재 timestamp와 expires_in의 값을 추가하여 session 변수에 access token expiry time을 저장해라.


## Step 3. If the access token expires , get a new access token

access token을 사용하는 Google API을 call하기 전에
access token expiry timestamp가 경과했는지 확인하라.
만약 그렇다면, refresh token을 사용하여 새로운 acccess token을 요청해라 


$data = $gapi->GetRefreshedAccessToken(CLIENT_ID, $_SESSION['refresh_token'], CLIENT_SECRET);


```
public function GetRefreshedAccessToken($client_id, $refresh_token, $client_secret) {   
    $url_token = 'https://www.googleapis.com/oauth2/v4/token';          
    
    $curlPost = 'client_id=' . $client_id . '&client_secret=' . $client_secret . '&refresh_token='. $refresh_token . '&grant_type=refresh_token';
    $ch = curl_init();      
    curl_setopt($ch, CURLOPT_URL, $url_token);      
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        
    curl_setopt($ch, CURLOPT_POST, 1);      
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);    
    $data = json_decode(curl_exec($ch), true);  //print_r($data);
    $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);      
    if($http_code != 200) 
        throw new Exception('Error : Failed to refresh access token');
        
    return $data;
}
```

코드를 보면 refresh token을 전달하여 새로운 access token을 구한다는 것을 알 수 있다. 

마찬가지로 새로운 access token의 expiry timestamp을 다시 저장해야 한다. 

