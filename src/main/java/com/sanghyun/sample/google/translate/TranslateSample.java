package com.sanghyun.sample.google.translate;


import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.google.cloud.translate.Detection;
import com.google.cloud.translate.Language;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.Translate.LanguageListOption;
import com.google.cloud.translate.Translate.TranslateOption;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;
import com.google.common.collect.ImmutableList;


public class TranslateSample {
	
	private List<String> languages = Arrays.asList(
	        "Afrikaans", "Albanian", "Amharic", "Arabic", "Armenian", "Azerbaijani", "Basque",
	        "Belarusian", "Bengali", "Bosnian", "Bulgarian", "Catalan", "Cebuano", "Chichewa",
	        "Chinese", "Chinese", "Corsican", "Croatian", "Czech", "Danish", "Dutch", "English",
	        "Esperanto", "Estonian", "Filipino", "Finnish", "French", "Frisian", "Galician",
	        "Georgian", "German", "Greek", "Gujarati", "Haitian", "Hausa", "Hawaiian", "Hebrew",
	        "Hindi", "Hmong", "Hungarian", "Icelandic", "Igbo", "Indonesian", "Irish", "Italian",
	        "Japanese", "Javanese", "Kannada", "Kazakh", "Khmer", "Korean", "Kurdish", "Kyrgyz",
	        "Lao", "Latin", "Latvian", "Lithuanian", "Luxembourgish", "Macedonian", "Malagasy",
	        "Malay", "Malayalam", "Maltese", "Maori", "Marathi", "Mongolian", "Myanmar", "Nepali",
	        "Norwegian", "Pashto", "Persian", "Polish", "Portuguese", "Punjabi", "Romanian",
	        "Russian", "Samoan", "Scots", "Serbian", "Sesotho", "Shona", "Sindhi", "Sinhala",
	        "Slovak", "Slovenian", "Somali", "Spanish", "Sundanese", "Swahili", "Swedish",
	        "Tajik", "Tamil", "Telugu", "Thai", "Turkish", "Ukrainian", "Urdu", "Uzbek",
	        "Vietnamese", "Welsh", "Xhosa", "Yiddish", "Yoruba", "Zulu");
	 
	 
	 /**
	   * Detect the language of input text.
	   *
	   * @param sourceText source text to be detected for language
	   * @param out print stream
	   */
	  //[START translate_detect_language]
	  public static void detectLanguage(String sourceText, PrintStream out) {
	    Translate translate = createTranslateService();
	    List<Detection> detections = translate.detect(ImmutableList.of(sourceText));
	    System.out.println("Language(s) detected:");
	    for (Detection detection : detections) {
	      out.printf("\t%s\n", detection);
	    }
	  }
	  //[END translate_detect_language]

	  /**
	   * Translates the source text in any language to English.
	   *
	   * @param sourceText source text to be translated
	   * @param out print stream
	   */
	  //[START translate_translate_text]
	  public static void translateText(String sourceText, PrintStream out) {
	    Translate translate = createTranslateService();
	    Translation translation = translate.translate(sourceText);
	    out.printf("Source Text:\n\t%s\n", sourceText);
	    out.printf("Translated Text:\n\t%s\n", translation.getTranslatedText());
	  }
	  //[END translate_translate_text]

	  /**
	   * Translate the source text from source to target language.
	   * Make sure that your project is whitelisted.
	   *
	   * @param sourceText source text to be translated
	   * @param sourceLang source language of the text
	   * @param targetLang target language of translated text
	   * @param out print stream
	   */
	  //[START translate_text_with_model]
	  public static void translateTextWithOptionsAndModel(
	      String sourceText,
	      String sourceLang,
	      String targetLang,
	      PrintStream out) {

	    Translate translate = createTranslateService();
	    TranslateOption srcLang = TranslateOption.sourceLanguage(sourceLang);
	    TranslateOption tgtLang = TranslateOption.targetLanguage(targetLang);

	    // Use translate `model` parameter with `base` and `nmt` options.
	    TranslateOption model = TranslateOption.model("nmt");

	    Translation translation = translate.translate(sourceText, srcLang, tgtLang, model);
	    out.printf("Source Text:\n\tLang: %s, Text: %s\n", sourceLang, sourceText);
	    out.printf("TranslatedText:\n\tLang: %s, Text: %s\n", targetLang,
	        translation.getTranslatedText());
	  }
	  //[END translate_text_with_model]


	  /**
	   * Translate the source text from source to target language.
	   *
	   * @param sourceText source text to be translated
	   * @param sourceLang source language of the text
	   * @param targetLang target language of translated text
	   * @param out print stream
	   */
	  public static void translateTextWithOptions(
	      String sourceText,
	      String sourceLang,
	      String targetLang,
	      PrintStream out) {

	    Translate translate = createTranslateService();
	    TranslateOption srcLang = TranslateOption.sourceLanguage(sourceLang);
	    TranslateOption tgtLang = TranslateOption.targetLanguage(targetLang);

	    Translation translation = translate.translate(sourceText, srcLang, tgtLang);
	    out.printf("Source Text:\n\tLang: %s, Text: %s\n", sourceLang, sourceText);
	    out.printf("TranslatedText:\n\tLang: %s, Text: %s\n", targetLang,
	        translation.getTranslatedText());
	  }

	  /**
	   * Displays a list of supported languages and codes.
	   *
	   * @param out print stream
	   * @param tgtLang optional target language
	   */
	  //[START translate_list_language_names]
	  //[START translate_list_codes]
	  public static void displaySupportedLanguages(PrintStream out, Optional<String> tgtLang) {
	    Translate translate = createTranslateService();
	    LanguageListOption target = LanguageListOption.targetLanguage(tgtLang.orElse("en"));
	    List<Language> languages = translate.listSupportedLanguages(target);

	    for (Language language : languages) {
	      out.printf("Name: %s, Code: %s\n", language.getName(), language.getCode());
	    }
	  }
	  //[END translate_list_codes]
	  //[END translate_list_language_names]

	  /**
	   * Create Google Translate API Service.
	   *
	   * @return Google Translate Service
	   */
	  public static Translate createTranslateService() {
	    return TranslateOptions.newBuilder().build().getService();
	  }

	  public static void main(String[] args) {
		  
		  String sourceText = "저는 빨리 집에가고 싶어요. 당신이 도와주면 안돼나요?.";
		  /// https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
		  String sourceLang = "ko";
		  String targetLang = "en";
		  TranslateSample.translateTextWithOptions(sourceText, sourceLang, targetLang, System.out);
		  
//	    String command = args[0];
//	    String text;
//
//	    if (command.equals("detect")) {
//	      text = args[1];
//	      TranslateSample.detectLanguage(text, System.out);
//	    } else if (command.equals("translate")) {
//	      text = args[1];
//	      try {
//	        String sourceLang = args[2];
//	        String targetLang = args[3];
//	        TranslateSample.translateTextWithOptions(text, sourceLang, targetLang, System.out);
//	      } catch (ArrayIndexOutOfBoundsException ex) {
//	    	  TranslateSample.translateText(text, System.out);
//	      }
//	    } else if (command.equals("langsupport")) {
//	      try {
//	        String target = args[1];
//	        TranslateSample.displaySupportedLanguages(System.out, Optional.of(target));
//	      } catch (ArrayIndexOutOfBoundsException ex) {
//	    	  TranslateSample.displaySupportedLanguages(System.out, Optional.empty());
//	      }
//	    }
		  
	  }//: main
}
