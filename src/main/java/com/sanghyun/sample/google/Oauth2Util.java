package com.sanghyun.sample.google;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.auth.oauth2.AuthorizationCodeTokenRequest;
import com.google.api.client.auth.oauth2.AuthorizationRequestUrl;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.sanghyun.core.util.ClassPathFileUtil;



/**
 * This class supports Authorization and Authentication for Google API using OAuth2 
 * 
 * @author Andy
 *
 */
public class Oauth2Util {
	
	private static final String APPLICATION_NAME = "Naonsoft Groupware";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";
	
	private static final String AUTH_URL = "https://accounts.google.com/o/oauth2/auth";
	private static final String CALLBACK_URL = "http://localhost/spring/view/sample/google/oauthcallback";
	private static final String TOKEN_REQ_URL = "https://www.googleapis.com/oauth2/v4/token"; 

	private static final String CLIENT_ID = "";
	private static final String CLIENT_SECRET = "";

	
	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR_READONLY);
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

	
	/**
	 * this redirects to Google and shows a consent screen
	 *  
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public static void showConsentScreen(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AuthorizationRequestUrl authUrl = new AuthorizationRequestUrl(AUTH_URL, CLIENT_ID, Arrays.asList("code"));
		authUrl.setRedirectUri(CALLBACK_URL);
		authUrl.set("access_type", "offline");
		authUrl.setScopes(Arrays.asList(CalendarScopes.CALENDAR));
		String redirectUri = authUrl.build();
		response.sendRedirect(redirectUri);
	}// :
	
	
	/**
	 * Gets a new refresh token using a saved refresh token 
	 * 
	 * @param refreshToken
	 * @return
	 * @throws Exception
	 */
	public static TokenResponse getRefreshToken(String refreshToken) throws Exception {
		
		// transport - HTTP transport
		// jsonFactory - JSON factory
		// refreshToken - refresh token issued to the client
		// clientId - client identifier issued to the client during the registration process
		// clientSecret - client secret
		TokenResponse tokenresponse = new GoogleRefreshTokenRequest(
				  new NetHttpTransport()
				, new JacksonFactory(),
				  refreshToken
				  ,CLIENT_ID
				  ,CLIENT_SECRET)
				.execute();
		return tokenresponse;
	}//:

	/**
	 * Gets Access Token from Google API 
	 * @param code
	 * @return
	 * @throws IOException
	 */
	public static TokenResponse getAccessToken(String code) throws IOException  {
		GenericUrl rurl = new GenericUrl(TOKEN_REQ_URL);
		rurl.set("client_id", CLIENT_ID);
		rurl.set("client_secret", CLIENT_SECRET);

		// AuthorizationCodeTokenRequest
		// Access Token Request에서 명시된 것처럼 인증코드(authorization code)를 사용하여
		// access token에 대한 OAuth 2.0 요청
		TokenResponse tokenresponse = new AuthorizationCodeTokenRequest(
				  new NetHttpTransport()
				, new JacksonFactory(),
				  rurl
				  ,code)
				.setRedirectUri(CALLBACK_URL)
				.execute();
		return tokenresponse;
	}//:

	
	/** 
	 * Gets a refresh token from a saved file
	 * 
	 * @param psnId
	 * @return
	 */
	public static String getRefreshTokenFromFile(String psnId)  {
		
		String tokenFileName = "tokens/" + psnId + ".token";
		try {
			String refreshToken = ClassPathFileUtil.readFile(tokenFileName);
			return refreshToken;
		}catch(Exception e) {
			return null; 
		}
	}//:
	
	
	
	/**
	 * Saves a refresh token with a person's ID
	 * @param psnId
	 * @param refreshToken
	 * @throws IOException
	 */
	private static void saveRefressToken(String psnId, String refreshToken) throws IOException  {
		
		URL url = SampleGoogleController.class.getProtectionDomain().getCodeSource().getLocation();
		String classPathRoot = url.getFile().substring(1);
		
		File file = new File(classPathRoot +  TOKENS_DIRECTORY_PATH);
		// if the directory doesn't exist, create the directory 
		if(!file.exists()) {
			if(!file.mkdir()) { 
				System.out.println("★★★★★★ Failed to make a directory.");
			}else { 
				System.out.println("★★★★★★ Succeeded to make a token directory.");
			}
		}
		String tokenFileName = classPathRoot + TOKENS_DIRECTORY_PATH + "/" + psnId + ".token";
		FileOutputStream fos  = new FileOutputStream(new File(tokenFileName));
		fos.write(refreshToken.getBytes());
		fos.close();
	}//:

	
	/**
	 * Gets a access token from Google API and saves a refresh token. 
	 * @param request
	 * @param response
	 * @param psnId
	 * @return
	 * @throws Exception
	 */
	public static TokenResponse getAccessToken(HttpServletRequest request, HttpServletResponse response, String psnId) throws Exception {
		String code = request.getParameter("code");
		TokenResponse token = null;
		token = getAccessToken(code); 
		saveRefressToken(psnId, token.getRefreshToken());
		return token;
	}// :
	

	  
}/// ~