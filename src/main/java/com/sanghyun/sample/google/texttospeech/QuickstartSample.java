package com.sanghyun.sample.google.texttospeech;


//Imports the Google Cloud client library
import com.google.cloud.texttospeech.v1.AudioConfig;
import com.google.cloud.texttospeech.v1.AudioEncoding;
import com.google.cloud.texttospeech.v1.SsmlVoiceGender;
import com.google.cloud.texttospeech.v1.SynthesisInput;
import com.google.cloud.texttospeech.v1.SynthesizeSpeechResponse;
import com.google.cloud.texttospeech.v1.TextToSpeechClient;
import com.google.cloud.texttospeech.v1.VoiceSelectionParams;
import com.google.protobuf.ByteString;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Google Cloud TextToSpeech API sample application.
 * Example usage: mvn package exec:java
 *                    -Dexec.mainClass='com.example.texttospeech.QuickstartSample'
 */
public class QuickstartSample {
	 /**
	   * Demonstrates using the Text-to-Speech API.
	   */
	  public static void main(String... args) throws Exception {
	    // Instantiates a client
	    try (TextToSpeechClient textToSpeechClient = TextToSpeechClient.create()) {
	      // Set the text input to be synthesized
	    	
	      //String text = "Stephen Biegun, the special representative for North Korea, said in a speech at Stanford University in California that the North has \"committed to the dismantlement and destruction\" of all its uranium and plutonium enrichment facilities both to U.S. Secretary of State Michael Pompeo and to leaders in Seoul.";
	      String text = "제의 드라마 'SKY 캐슬'은 the special representative for North Korea 대한민국 상위 0.1%가 모여 사는 SKY 캐슬 안에서 남편은 왕으로, 제 자식은 천하제일 왕자와 공주로 키우고 싶은 명문가 출신 사모님들의 처절한 욕망을 샅샅이 들여다보는 리얼 코믹 풍자극이다.";
	      
	      SynthesisInput input = SynthesisInput.newBuilder()
	            //.setText("Hello, World!")
	    		  .setText(text)
	            .build();

	      // Build the voice request, select the language code ("en-US") and the ssml voice gender
	      // ("neutral")
	      VoiceSelectionParams voice = VoiceSelectionParams.newBuilder()
	          //.setLanguageCode("en-US")
	          .setLanguageCode("ko-KR")
	          .setSsmlGender(SsmlVoiceGender.NEUTRAL)
	          .build();

	      // Select the type of audio file you want returned
	      AudioConfig audioConfig = AudioConfig.newBuilder()
	          .setAudioEncoding(AudioEncoding.MP3)
	          .build();

	      // Perform the text-to-speech request on the text input with the selected voice parameters and
	      // audio file type
	      SynthesizeSpeechResponse response = textToSpeechClient.synthesizeSpeech(input, voice,
	          audioConfig);

	      // Get the audio contents from the response
	      ByteString audioContents = response.getAudioContent();

	      // Write the response to the output file.
	      try (OutputStream out = new FileOutputStream("e:/tmp/output.mp3")) {
	        out.write(audioContents.toByteArray());
	        System.out.println("Audio content written to file \"output.mp3\"");
	      }
	    }
	  }	
}
