package com.sanghyun.sample.google;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.AuthorizationCodeTokenRequest;
import com.google.api.client.auth.oauth2.AuthorizationRequestUrl;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.BasicAuthentication;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.CalendarScopes;
import com.sanghyun.core.base.BaseController;

@Controller
@RequestMapping(value = "/{sitemesh}/sample/google")
public class SampleGoogleController extends BaseController    {
	
    private static final String psnId = "P123456789";

    /** Sample Google Service */
    @Autowired
    private SampleGoogleService sampleGoogleService;
    
    
    
    /**
     * Redirects to Google's consent screen
     * 
     * @param request
     * @param response
     * @throws Exception
     */
	@RequestMapping(value="/oauthlogin")
	public void oauthLogin(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		String refreshTokenString = Oauth2Util.getRefreshTokenFromFile(psnId);
		if(refreshTokenString == null || refreshTokenString.length() == 0 ) {
			// if a refresh token doesn't exist, this method will redirect to a consent screen			
			Oauth2Util.showConsentScreen(request, response);	
		}else { 
			// if a refresh token exists
			// execute synchronization
			sampleGoogleService.syncCalendar(request, response, Oauth2Util.getRefreshToken(refreshTokenString));
		}
	}//:	
	
	
	/**
	 * If a user admit, Google redirects to this uri
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/oauthcallback")
	public void oauthCallback(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		
		String error = request.getParameter("error");
		
		if (error != null) {
			System.out.println("The user didn't authroize.");
			// TODO : 에러처리 
			return;
		}
		String code = request.getParameter("code");
		if(code == null) {
			System.out.println("The user didn't authroize");
			// TODO : 에러처리
			return; 
		}
		
		sampleGoogleService.syncCalendar(request,response, Oauth2Util.getAccessToken(request,response, psnId));
	}//:
	
}///~

