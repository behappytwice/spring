package com.sanghyun.sample.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

@Component
public class SampleJmsMessageSender {

	  @Autowired
	  private JmsTemplate jmsTemplate;

	  public void send(String destination, String message) {
	    jmsTemplate.convertAndSend(destination, message);
	  }
	  
	  
	  public void send(final Destination dest, final String text) {
		  this.jmsTemplate.send(dest, new MessageCreator() {

			@Override
			public Message createMessage(Session session) throws JMSException {
				Message message = session.createTextMessage(text);
				return message;
			}
		  });
	  }//:

	  
	  public void send(final Destination dest, final SampleJmsBean bean) {
		  jmsTemplate.convertAndSend(dest, bean);
		  
//		  this.jmsTemplate.send(dest, new MessageCreator() {
//			@Override
//			public Message createMessage(Session session) throws JMSException {
//				ObjectMessage message = session.createObjectMessage(bean);
//				return message;
//			}
//		  });
	  }//:
	  
	  
}///~
