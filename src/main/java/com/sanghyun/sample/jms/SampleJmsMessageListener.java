package com.sanghyun.sample.jms;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;
 

@Component
public class SampleJmsMessageListener {

	private static final String ORDER_RESPONSE_QUEUE = "order-queue";
	/*
	//@JmsListener(destination = ORDER_RESPONSE_QUEUE)
    @JmsListener(destination = "${activemq.queue.name}")
    public void receiveMessage(final Message<SampleJmsBean> message) throws JMSException {

    	if(message instanceof ObjectMessage) {
    		System.out.println(">>>>>>>>>>> OBJECT MESSAGE");
    		ObjectMessage objectMessage = (ObjectMessage)message;
    		SampleJmsBean bean = (SampleJmsBean)objectMessage.getObject();
    		if(null != bean) {
    			System.out.println(bean.getUserName());
    		}
    		
    	}else if (message instanceof TextMessage) {
    		TextMessage txtMsg = (TextMessage)message;
    		String text = txtMsg.getText();
    		System.out.println("Text Message:" + text);
            //MessageHeaders headers =  txtMsg.getHeaders();
            //String response = txtMsg.getPayload();
    	}
    }//:
*/    
    
//    @JmsListener(destination = "${activemq.queue.name}")
//    public void receiveMessage(String message) throws JMSException {
//
//    	System.out.println(">>>> String message: " + message);
//    }//:

    
    @JmsListener(destination = "${activemq.queue.name}")
    public void receiveMessage(SampleJmsBean  bean) throws JMSException {
    	System.out.println(">>>> SampleJmsBean");
    	System.out.println(bean);
    }//:

    
    
}///~