package com.sanghyun.sample.jms;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;


public class SampleJmsObjectMessageListener {

	private static final String ORDER_RESPONSE_QUEUE = "order-queue";

	// @JmsListener(destination = ORDER_RESPONSE_QUEUE)
	@JmsListener(destination = "${activemq.queue.name}")
	public void receiveMessage(final Message message) throws JMSException {
		
		System.out.println(">>>>>>>>>>> OBJECT MESSAGE");
		
		if(message instanceof TextMessage) { return; }
		
		ObjectMessage objectMessage = (ObjectMessage)message;
		SampleJmsBean bean = (SampleJmsBean)objectMessage.getObject();
		if(null != bean) {
			System.out.println(bean.getUserName());
		}

	}// :

}/// ~