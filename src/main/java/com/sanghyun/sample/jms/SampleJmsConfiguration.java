package com.sanghyun.sample.jms;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.jms.annotation.EnableJms;

//@Configuration
//@EnableJms
public class SampleJmsConfiguration {
	
	@Bean
	@Lazy
	//@DependsOn("jmsListenerContainerFactory")
	public SampleJmsMessageListener receiver() {
		return new SampleJmsMessageListener();
	}
	
	
//	@Bean
//	@Lazy
//	//@DependsOn("jmsTemplate")
//	public SampleJmsMessageSender sender() {
//		return new SampleJmsMessageSender();
//	}

}
