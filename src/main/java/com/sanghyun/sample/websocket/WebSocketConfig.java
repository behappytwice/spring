package com.sanghyun.sample.websocket;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;


//@Configuration
//@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		//registry.addHandler(myHandler(), "/myHandler");  // WebSocket 사용시
		registry.addHandler(myHandler(), "/myHandler").withSockJS();  // SockJS 클라이언트 사용시
	}

	@Bean
	public WebSocketHandler myHandler() {
		return new MyWebSocketHandler();
	}

}///~