package com.sanghyun.sample.websocket.stomp;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;


@Controller
public class GreetingController {
	// WebSocketConfig에서 설정한 destinationprefix + 클라이언트가 전송할 mapping 주소
	// 클라이언트가 /app/in을 붙여 서버로 메시지를 전달한다.
	@MessageMapping("/in/tt")
	// /topic/in을 구독하고 있는 클라이언트에게 데이터를 전송한다 
	@SendTo("/topic/in")
	public Greeting greeting(HelloMessage message) throws Exception {
		Thread.sleep(1000); // simulated delay
		System.out.println("==★★★★★★★★★★★");
		return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
	}
}//~
