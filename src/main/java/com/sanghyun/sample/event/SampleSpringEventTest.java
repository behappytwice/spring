package com.sanghyun.sample.event;

import java.lang.reflect.Method;
import java.util.Set;

import org.junit.Test;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import com.sanghyun.core.annotation.TrxAnnotation;
import com.sanghyun.core.junit.BaseJunitTest;

public class SampleSpringEventTest extends BaseJunitTest {
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@Test
	public void testPublishEvent() throws Exception {
		for (int i = 0; i < 100; i++) {

			String message = "This is a sample spring message." + String.valueOf(i);
			System.out.println("Publishing custom event. " + String.valueOf(i));

			SampleSpringEvent customSpringEvent = new SampleSpringEvent(this, message);
			applicationEventPublisher.publishEvent(customSpringEvent);
			Thread.sleep(500);
		}
	}// ~
	
	@Test
	public void testDisplayAllBeans() {
		//https://www.baeldung.com/java-custom-annotation 
		// ㄴ annotation을 찾는 방법 
		//https://www.baeldung.com/spring-show-all-beans
		// 모든 Spring beans을 가져온다 
		String[] allBeanNames = applicationContext.getBeanDefinitionNames();
		for (String beanName : allBeanNames) {
			System.out.println(beanName);
		}
	}
	
	// https://www.baeldung.com/spring-component-scanning
	// https://www.baeldung.com/java-custom-annotation
	// Spring Component Scan 
	@Test
	public void testComponentScan() {
		String[] allBeanNames = applicationContext.getBeanDefinitionNames();
		for (String beanName : allBeanNames) {
			Object o = applicationContext.getBean(beanName);
			Class<?> clazz = o.getClass();
			// Spring @Service annotation 적용된 클래스 찾기 
		    if (clazz.isAnnotationPresent(org.springframework.stereotype.Service.class)) {
		    	System.out.println("BEAN NAME:" + beanName + "//" + o.getClass().toString());
		    	for (Method method : clazz.getDeclaredMethods()) {
		            if (method.isAnnotationPresent(TrxAnnotation.class)) {
		                //method.setAccessible(true);  // 언제 쓰는 건지 확인필요함 
		                System.out.println("MethodName=" + method.getName());
		            }
		        }
		    }	
		}
	}//
	
	@Test
	public void testDisplayAnnotations() {
		//https://github.com/ronmamo/reflections
		Reflections reflections = new Reflections("com.sanghyun");
		Set<Class<?>> interceptors = 
			    reflections.getTypesAnnotatedWith(com.sanghyun.core.annotation.InterceptorAnnotation.class);
		Object[] array = interceptors.toArray();
		for(Object o : array) {
		}
	}

	
}// ~
