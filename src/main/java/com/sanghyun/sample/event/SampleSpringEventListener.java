package com.sanghyun.sample.event;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component 
public class SampleSpringEventListener implements ApplicationListener<SampleSpringEvent> {
	@Override
	public void onApplicationEvent(SampleSpringEvent event) {
		// TODO Auto-generated method stub
		System.out.println("Got a event of SampleEvent");
	}
}
