package com.sanghyun.sample.event;

import org.springframework.context.ApplicationEvent;

public class SampleSpringEvent extends ApplicationEvent {
	static final long serialVersionUID = 1L;
	private String message;

	public SampleSpringEvent(Object source, String message) {
		super(source);
		this.message = message;
	}
}//~