package com.sanghyun.sample.sitemesh;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sanghyun.core.base.BaseController;


@Controller
@RequestMapping(value="/{sitemesh}/sample/sitemesh")
public class SampleSitemeshController extends BaseController {

	
	@RequestMapping(value="/main")
	public ModelAndView createView() {
		ModelAndView mv = new ModelAndView("sample/sitemesh/ftlSitemesh");
		return mv; 
	}//:
	
}//


