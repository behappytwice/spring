package com.sanghyun.sample.enumtest;

public enum LevelEnum {

	HIGH(3),
	MEDIUM(2),
	LOW(1)
	;

	private final int levelCode;
	LevelEnum(int levelCode) {
		this.levelCode = levelCode;
	}
	public int getLevelCode() {
		return this.levelCode;
	}
}
