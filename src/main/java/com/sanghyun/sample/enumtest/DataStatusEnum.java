package com.sanghyun.sample.enumtest;

public enum DataStatusEnum {
	CREATED("C"),
	DELETED("D"),
	MODIFIED("M");
	
	private final String statusCode;
	DataStatusEnum(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusCode() {
		return this.statusCode;
	}

}
