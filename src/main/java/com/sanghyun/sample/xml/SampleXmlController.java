package com.sanghyun.sample.xml;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sanghyun.core.base.BaseController;

@Controller
@RequestMapping(value="/{sitemesh}/sample/xml")
public class SampleXmlController extends BaseController  {
	
	@RequestMapping(value="/fetchEmployee", produces="application/xml")
	public @ResponseBody SampleXmlEmployee fetchEmployee() {
		
		SampleXmlEmployee emp = new SampleXmlEmployee();
		
		emp.setName("Sanghyun"); 
		emp.setUserId("abcdefg");
		emp.setPassword("123456");
		
		return emp; 
		
	}//:

	
	@RequestMapping(value="/fetchEmployeeWithJson", produces="application/json")
	public @ResponseBody SampleXmlEmployee fetchEmployeeWithJson() {
		
		SampleXmlEmployee emp = new SampleXmlEmployee();
		
		emp.setName("Sanghyun"); 
		emp.setUserId("abcdefg");
		emp.setPassword("123456");
		
		return emp; 
		
	}//:

}///~
