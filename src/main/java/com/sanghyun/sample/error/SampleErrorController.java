package com.sanghyun.sample.error;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sanghyun.core.base.BaseController;

@Controller
@RequestMapping(value = "/{sitemesh}/sample/error")
public class SampleErrorController extends BaseController {  
	
	@RequestMapping(value = "/internalError")
	public void internalError() {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> internalError ");
		throw new RuntimeException("internal Error");
	}//:
	
}///~
