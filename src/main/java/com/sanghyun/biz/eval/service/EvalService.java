package com.sanghyun.biz.eval.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sanghyun.biz.eval.beans.EvalUser;
import com.sanghyun.core.annotation.TrxAnnotation;

/** 평가 서비스 */
@Service
public class EvalService {

	@TrxAnnotation(trxId = "ABCDEFGHHHH1000")
	public List<EvalUser> getUsers() {
		List<EvalUser> list = new ArrayList<EvalUser>();
		
		EvalUser user = new EvalUser();
		user.setName("Kim");;
		user.setUserId("aa111");
		list.add(user);
		
		user = new EvalUser();
		user.setName("Kim");;
		user.setUserId("aa111");
		list.add(user);
		
		return list; 
	}
}///~
