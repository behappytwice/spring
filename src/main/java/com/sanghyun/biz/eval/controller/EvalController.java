package com.sanghyun.biz.eval.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sanghyun.biz.eval.beans.EvalUser;
import com.sanghyun.biz.eval.service.EvalService;
import com.sanghyun.core.base.BaseController;

/** 평가 컨트롤러 */
@Controller
@RequestMapping(value="/{sitemesh}/eval")
public class EvalController extends BaseController {

	@Autowired
	private EvalService evalService; 
	
	/** 평가 main 화면 호출 */
	@RequestMapping(value="/main")
	public ModelAndView createView() {
		ModelAndView mv = new ModelAndView("biz/eval/ftlEvalMain");
		return mv; 
	}//:
	
	@RequestMapping(value="/getUsers")
	public @ResponseBody List<EvalUser> getUsers() {
		return evalService.getUsers();
	}
	
}///~
