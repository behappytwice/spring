package com.sanghyun.exam;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.sanghyun.exam.beans.CalcBase;
import com.sanghyun.exam.beans.Calculator;

public class UserTest {

	/** 난이도 ★ */
	@Test
	public void test1() {
		User a = new User("kim", 30);
		User b = new User("kim", 30);
		User c = a;
		System.out.println(a == b);
		System.out.println(a == c);
		System.out.println(a.equals(b));
		// 실행결과는?
		// true, true, true
		// true, false, true
		// false, true, true
		// false, true, false (O)
	}// :

	/** 난이도 ★ */
	@Test
	public void test2() {

		int total = 0;
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				total++;
			}
		}
		System.out.println(total);
		// 답 [ 15]
	}// :

	/** 난이도 ★ */
	@Test
	public void test3() {
		String filePath = "c://user.txt";
		File inputFile = new File(filePath);
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(inputFile);
			while (inputStream.available() > 0) {
				System.out.print((char) inputStream.read());
			}
		} catch (IOException e) {
			try {
				inputStream.close();
			} catch (Exception e1) {
			}
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e1) {
			}
		}
	}// :

	/** 난이도 ★ */
	@Test
	public void test4() {
		char c = 'A';
		char d = (char) (c + 1);
		System.out.println(d);
	}// :

	/** 난이도 ★ */
	@Test
	public void test5() {

		int numOfStudents = 35;
		int sizeOfBus = 10;
		int numOfBuses = numOfStudents / sizeOfBus + ((numOfStudents % sizeOfBus) > 0 ? 1 : 0);
		System.out.println(numOfBuses);
		// 정답 4

	}// :

	/** 난이도 ★ */
	@Test
	public void test6() {
		int num = 444;
		int result = num / 100 * 100;
		System.out.println(result);

	}// :

	public int sum(int n) {
		if (n == 100)
			return 100;
		return n + sum(n + 1);
	}

	@Test
	public void test7() {
		System.out.println(sum(1));
	}

	@Test
	public void test8() {

		Map<String, Integer> vehicles = new HashMap<String, Integer>();
		// Add some vehicles.
		vehicles.put("BMW", 5);
		vehicles.put("Mercedes", 3);
		vehicles.put("Audi", 4);
		vehicles.put("Ford", 10);
		System.out.println("Total vehicles: " + vehicles.size());
		// Iterate over all vehicles, using the keySet method.
		for (String key : vehicles.keySet())
			System.out.println(key + " - " + vehicles.get(key));
		System.out.println();
		String searchKey = "Audi";
		if (vehicles.containsKey(searchKey))
			System.out.println("Found total " + vehicles.get(searchKey) + " " + searchKey + " cars!\n");
		// Clear all values.
		vehicles.clear();
		// Equals to zero.
		System.out.println("After clear operation, size: " + vehicles.size());
	}

	@Test
	public void test9() {
		List<String> list = new ArrayList<>();
		list.add("사과");
		list.add("배");
		list.add("파인애플");
		Iterator iter = list.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}
	}

	final static short x = 2;
	public static int y = 0;

	@Test
	public void test10() {

		// int z = 2;
		//
		// switch (z)
		// {
		// case x: System.out.print("0 "); // 2 below
		// default: System.out.print("def ");
		// case x-1: System.out.print("1 "); // 1
		// break;
		// case x-2: System.out.print("2 "); // 0
		// }

		for (int z = 0; z < 4; z++) {
			// System.out.println("z=" + String.valueOf(z));
			switch (z) {
			case x:
				System.out.print("0 "); // 2 below
			default:
				System.out.print("def ");
			case x - 1:
				System.out.print("1 "); // 1
				break;
			case x - 2:
				System.out.print("2 "); // 0
			}
		}
	}// :

	public void leftshift(int i, int j) {
		i <<= j;
		System.out.println(i);
	}
	
	@Test
	public void test11() {
		leftshift(1,2);
	}
	
	@Test
	public void test12() {
		int i=0; 
		for( i=1; i <= 3; i++) {
			
		}
		System.out.println(i);
	}//:
	
	@Test
	public void test13() {
		
		Calculator calc = new Calculator();
		System.out.println(calc.sum(1, 1));
		System.out.println(calc.sum(1, 1, 1));
		System.out.println(calc.sum(1L, 1L, 1L));
		CalcBase calBase = calc;
		System.out.println(calBase.toString());
		
	}

}/// ~
