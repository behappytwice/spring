package com.sanghyun.exam.beans;

public class CalcBase {

	public int sum(int a, int b) { 
		return a + b; 
	}
	public int sum(int a, int b, int c) {
		return a + b + c; 
	}

	public String toString() { 
		return "CalcBase";
	}
}
