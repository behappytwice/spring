package com.sanghyun.test.rsa;

import java.security.Key;
import java.util.Date;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.junit.Test;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;

public class RSATest {

	private SecretKey key;
	
	@Test
	public void test() {

	}

	@Test
	public void testGenerateToken() {

		// We need a signing key, so we'll create one just for this example. Usually
		// the key would be read from your application configuration instead.
		Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

		// String jws = Jwts.builder().setSubject("Joe").signWith(key).compact();
		// System.out.println(jws);

		String jws2 = Jwts.builder().setHeaderParam("myname", "kim").setHeaderParam("myage", "30").setSubject("Joe")
				.signWith(key).claim("user", "sanghyun").compact();
		System.out.println(jws2);

		if (Jwts.parser().setSigningKey(key).parseClaimsJws(jws2).getBody().getSubject().equals("Joe")) {
			System.out.println("True");
		} else {
			System.out.println("False");
		}

	}
	
	public void createKey() {
		SecretKey key2 = Keys.secretKeyFor(SignatureAlgorithm.HS256); //or HS384 or HS512
		String algorrithm = key2.getAlgorithm();
		// SecretKey to String 
		String encodedKey = Encoders.BASE64.encode(key2.getEncoded());
		System.out.println("encodedKey=" + encodedKey);
		// String to SecretKey
		byte[] decodedKey = Decoders.BASE64.decode(encodedKey);
		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, algorrithm); 
		this.key = originalKey;
	}//:
	
	
	public String createJWT(String id, String issuer, String subject, long ttlMillis) {
		
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);
		
		//key = Keys.secretKeyFor(SignatureAlgorithm.HS256); //or HS384 or HS512
		

		// Let's set the JWT Claims
		JwtBuilder builder = Jwts.builder()
				.setId(id)
				.setIssuedAt(now)
				.setSubject(subject) 
				.setIssuer(issuer)
				.signWith(key);

		// if it has been specified, let's add the expiration
		if (ttlMillis >= 0) {
			long expMillis = nowMillis + ttlMillis;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}

		// Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}//:
	
	//Sample method to validate and read the JWT
	private void parseJWT(String jwt) {
	 
	    //This line will throw an exception if it is not a signed JWS (as expected)
	    Claims claims = Jwts.parser()         
	       .setSigningKey(key)
	       .parseClaimsJws(jwt).getBody();
	    
	    System.out.println("ID: " + claims.getId());
	    System.out.println("Subject: " + claims.getSubject());
	    System.out.println("Issuer: " + claims.getIssuer());
	    System.out.println("Expiration: " + claims.getExpiration());
	}
	//
	@Test 
	public void testJjwtCreateAndParse() {
		this.createKey();
		String jws = this.createJWT("me", "me", "for test", 1000 * 60 * 60);
		this.parseJWT(jws);
	}

	@Test
	public void testJjwt() {
		// For Java 8 
		//https://stackoverflow.com/questions/5355466/converting-secret-key-into-a-string-and-vice-versa
		
		SecretKey key2 = Keys.secretKeyFor(SignatureAlgorithm.HS256); //or HS384 or HS512
		// SecretKey to String 
		String encodedKey = Encoders.BASE64.encode(key2.getEncoded());
		System.out.println("encodedKey=" + encodedKey);
		// String to SecretKey
		byte[] decodedKey = Decoders.BASE64.decode(encodedKey);
		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
		
		String mykey = "나는 행복한 사람.";
		String encodedUrl = Encoders.BASE64URL.encode(mykey.getBytes());
		System.out.println(encodedUrl);
		byte[] decodedBytes = Decoders.BASE64URL.decode(encodedUrl);
		System.out.println(new String(decodedBytes));
		
	}//:

}
