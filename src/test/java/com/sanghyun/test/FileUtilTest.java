package com.sanghyun.test;

import java.net.URL;
import java.util.TimeZone;

import org.junit.Test;

import com.sanghyun.sample.google.SampleGoogleController;

public class FileUtilTest {


	@Test
	public void test() {
		
//		String path = "E:/mytemp";
//		File file = new File(path);
//		file.mkdir();
//		
		URL url = SampleGoogleController.class.getProtectionDomain().getCodeSource().getLocation();
		String classPathRoot = url.getFile();
		System.out.println("URL:" +	classPathRoot.substring(1));
	}
	
	
	
	@Test
	public void testTimeZone() {
		
		String[] timeZoneArr = TimeZone.getAvailableIDs();
		int i=0;
		for (String timeZone : timeZoneArr) {
			System.out.print(Integer.toString(i++));
		    System.out.println(timeZone);
		}
		
	}

}
