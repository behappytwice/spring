package com.sanghyun.sample.enumtest;

import static org.junit.Assert.*;

import org.junit.Test;

public class EnumTestCase {

	@Test
	public void test() {
		
		// enum 값을 할당하고 
		BasicEnum basic = BasicEnum.JAN;
		// if 구문에서 비교할 수 있음
		if(basic == BasicEnum.JAN) {
			System.out.println("EQUAL");
		}else { 
			System.out.println("NOT EQUAL");
		}
		// 할당하지 않고 그냥 출력하면
		// "JAN"이 코솔에 찍힘
		System.out.println(BasicEnum.JAN);
		// toSring()으로 출력하면 "JAN"이 출력됨 
		System.out.println(BasicEnum.JAN.toString());
		
	}//:
	
	
	@Test
	public void test2() {
		// HIGH가 출력된다.
		System.out.println(LevelEnum.HIGH);
		// HIGH가 출력된다.
		System.out.println(LevelEnum.HIGH.toString());
		// HIGH가 출력된다.
		System.out.println(LevelEnum.HIGH.valueOf("HIGH"));
		// HIGH가 출력된다
		System.out.println(LevelEnum.HIGH.name());
		
		LevelEnum level = LevelEnum.HIGH;
		// HIGH가 출력된다
		System.out.println(level);
		
		// EQUAL이 출력된다.
		if(level == LevelEnum.HIGH) {
			System.out.println("EQUAL");
		}else { 
			System.out.println("NOT EQUAL");
		}
		// 3이 출력된다
		System.out.println(level.getLevelCode());
	}
	
	@Test
	public void test3() {
		
		DataStatusEnum dstat = DataStatusEnum.CREATED;
		// CREATED가 출력된다 
		System.out.println(dstat);
		// "C"가 출력된다
		System.out.println(dstat.getStatusCode());
		// EQUAL이 출력된다
		if(dstat == DataStatusEnum.CREATED) {
			System.out.println("EQUAL");
		}else { 
			System.out.println("NOT EQUAL");
		}
		// M이 출력됨
		System.out.println(DataStatusEnum.MODIFIED.getStatusCode());

		/*
		DataStatusEnum stat = DataStatusEn
		System.out.println(stat);
		*/
		
	}

}
