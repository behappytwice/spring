이것은 당신이 생성해 왔던 어떠한 다른 것들과 같은 컴포넌트(component)이고, 그것은 모듈이기 때문에 구성 오브젝트(configuration object)를 export만 한다. 


`SingleFileComponent.js `

```javascript 
export default {
  template: `
    <div>
     <h1>Single-file JavaScript Component</h1>
     <p>{{ message }}</p>
    </div>
  `,
  data() {
    return {
      message: 'Oh hai from the component'
    }
  }
}

```

이제 그것을 import하고 Vue app에서 사용한다.    

`app.js`   

```javascript

import SingleFileComponent from 'SingleFileComponent.js';
new Vue({
  el: '#app',
  components: {
    SingleFileComponent
  }
});

```

> from에 './SingFileComponent.js'를 하지 않으면 톰캣 구성 시에 가져오지 않는다. 자세한 사항은 확인을 더 할 필요가 있다. 


`index.html`
```
<div id="app">
  <single-file-component></single-file-component>
</div>
```

