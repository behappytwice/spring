# @Profile 설정

개발환경, 검증환경, 운영환경에 따라 DB 접속 방법 및 디렉토리 경로를 다르게 설정해야 하는 경우 스프링에서 제공하는 Profile 기능을 사용한다.  profile을 사용하려면 스프링의 속성인 **spring.profiles.active** 를 사용한다.




## web.xml 이용  

```
<context-param>
    <param-name>spring.profiles.active</param-name>
    <param-value>dev</param-value>
</context-param>

```



## jvm property로 전달 


```
-Dspring.profiles.active=dev,profile2
```


### JUnit에서 설정하는 방법

```java
@ActiveProfiles("dev")
public class TransferServiceTest {
}

```

### WebApplicationInitializer에서 설정 방법

onStartup 메서드에서 System.setProperty를 이용하여 설정

```java

public class WebMVCInitializer implements WebApplicationInitializer {

public void onStartup(ServletContext servletContext) throws ServletException {
    if(System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME) == null) {
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, 
"development");
}
        
}


```
### Profile을 이용하여 설정파일(.properties)을 읽는 방법

Property PlaceHolder를 이용하여 값을 이용할 수 있다

```java
@PropertySources(
@PropertySource(value = "classpath:profile/${spring.profiles.active}/prod.properties", 
          ignoreResourceNotFound = true) 
})
@EnableTransactionManagement 
public class RootConfig {

```
### Profile을 이용하여 Bean을 생성하는 방법

인터페이스를 하나 만든다 

```java
public interface TestProfileInterface {

    String getName();
    void setName(String name);
}

```

인터페이스 구현체를 두 개 만든다.  “product” 환경에서 사용할 Class를 생성한다.


```java
public class TestProfileBeanProduct implements TestProfileInterface{
    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "productName";
    }
    @Override
    public void setName(String name) {
        // TODO Auto-generated method stub
        
    }
}
```
“development” 환경에서 사용할 class를 만든다

```java
public class TestProfileBeanDevelopment implements  TestProfileInterface{

    private String name;

    public String getName() {
        return name + "development";
    }

    public void setName(String name) {
        this.name = name;
    } 
    
}

```

Configuration 클래스에 @Bean을 사용할 때 @Profile을 사용하여 생성하는 Bean을 결정할 수 있다.



```java
@Configuration
public class RootConfig {
    
    
    @Bean
    @Profile("development")
    public TestProfileInterface testProfileBean() {
        return new TestProfileBeanDevelopment();
    }
    @Bean
    @Profile("product")
    public TestProfileInterface testProfileBeanProduct() {
        return new TestProfileBeanProduct();
    }

```

### Profile이 존재하는지 확인하는 방법

env.getActiveProfiles()을 이용하여 설정된 프로파일의 값을 읽을 수 있다. 

```java
public static boolean isProfileExists(Environment env, String profile) {
        if(Arrays.stream(env.getActiveProfiles()).anyMatch(  envl -> (  envl.equalsIgnoreCase(profile) ))) 
        {
            return true;
        }
        return false; 
    }

```



### 참조
[링크1](https://examples.javacodegeeks.com/enterprise-java/spring/load-environment-configurations-and-properties-with-spring-example/)
[링크2](http://jdm.kr/blog/81)
[링크3](https://www.lesstif.com/pages/viewpage.action?pageId=18220309)
[링크4](https://www.mkyong.com/spring/spring-profiles-example/)
[링크5](https://codingexplained.com/coding/java/spring-framework/enabling-spring-bean-for-multiple-profiles)




