# @ResponseBody

[링크](https://memorynotfound.com/spring-mvc-download-file-examples/)


## ResponseBody byte[] – Image/Media Data를 반환하기

[링크](http://www.baeldung.com/spring-mvc-image-media-data)

Spring MVC framework를 이용하여 image와 다른 media를 반환하는 방법으로 Message Conversion, Content Negotiation을 이용한다. 
ByteArrayHttpMessageConverter Bean을 생성해야 한다. 

```java
@Bean
    public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        ByteArrayHttpMessageConverter arrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
        arrayHttpMessageConverter.setSupportedMediaTypes(getSupportedMediaTypes());
        return arrayHttpMessageConverter;
    }

    private List<MediaType> getSupportedMediaTypes() {
        List<MediaType> list = new ArrayList<MediaType>();
        list.add(MediaType.IMAGE_JPEG);
        list.add(MediaType.IMAGE_PNG);
        list.add(MediaType.APPLICATION_OCTET_STREAM);
        return list;
    }
```


생성된 Bean을 등록해야 한다. 
```java

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
        messageConverters.add(httpMessageConverter());
        messageConverters.add(jacksonJsonConverter());
        messageConverters.add(byteArrayHttpMessageConverter());
        super.configureMessageConverters(messageConverters);
    }




    @RequestMapping("/download2")
    public @ResponseBody byte[] download2(HttpServletRequest request, HttpServletResponse response) {
        try { 
            String fileName = "E:/park.png";
            InputStream in = null;
            in = new FileInputStream(new File(fileName));
            return IOUtils.toByteArray(in);
        }catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }//:

````


