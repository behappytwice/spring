# @RequestBody

JSON이나 XML로 전달된 Http 요청 데이터의 body를 읽어온다. 



```java
public void test(@RequestBody String bol) {

```

val이라는 변수에 body의 값이 그대로 세팅 

```java
public void test(@RequestBody UserDto user) {
```
user 객체에 매핑한다.
